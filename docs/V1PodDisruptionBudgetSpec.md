# V1PodDisruptionBudgetSpec

PodDisruptionBudgetSpec is a description of a PodDisruptionBudget.

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**max_unavailable** | **object** | An eviction is allowed if at most \&quot;maxUnavailable\&quot; pods selected by \&quot;selector\&quot; are unavailable after the eviction, i.e. even in absence of the evicted pod. For example, one can prevent all voluntary evictions by specifying 0. This is a mutually exclusive setting with \&quot;minAvailable\&quot;. | [optional] 
**min_available** | **object** | An eviction is allowed if at least \&quot;minAvailable\&quot; pods selected by \&quot;selector\&quot; will still be available after the eviction, i.e. even in the absence of the evicted pod.  So for example you can prevent all voluntary evictions by specifying \&quot;100%\&quot;. | [optional] 
**selector** | [**V1LabelSelector**](V1LabelSelector.md) |  | [optional] 

## Example

```python
from pyk8s.models.v1_pod_disruption_budget_spec import V1PodDisruptionBudgetSpec

# TODO update the JSON string below
json = "{}"
# create an instance of V1PodDisruptionBudgetSpec from a JSON string
v1_pod_disruption_budget_spec_instance = V1PodDisruptionBudgetSpec.from_json(json)
# print the JSON string representation of the object
print V1PodDisruptionBudgetSpec.to_json()

# convert the object into a dict
v1_pod_disruption_budget_spec_dict = v1_pod_disruption_budget_spec_instance.to_dict()
# create an instance of V1PodDisruptionBudgetSpec from a dict
v1_pod_disruption_budget_spec_form_dict = v1_pod_disruption_budget_spec.from_dict(v1_pod_disruption_budget_spec_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)



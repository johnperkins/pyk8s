# V2beta2ObjectMetricStatus

ObjectMetricStatus indicates the current value of a metric describing a kubernetes object (for example, hits-per-second on an Ingress object).

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**current** | [**V2beta2MetricValueStatus**](V2beta2MetricValueStatus.md) |  | 
**described_object** | [**V2beta2CrossVersionObjectReference**](V2beta2CrossVersionObjectReference.md) |  | 
**metric** | [**V2beta2MetricIdentifier**](V2beta2MetricIdentifier.md) |  | 

## Example

```python
from pyk8s.models.v2beta2_object_metric_status import V2beta2ObjectMetricStatus

# TODO update the JSON string below
json = "{}"
# create an instance of V2beta2ObjectMetricStatus from a JSON string
v2beta2_object_metric_status_instance = V2beta2ObjectMetricStatus.from_json(json)
# print the JSON string representation of the object
print V2beta2ObjectMetricStatus.to_json()

# convert the object into a dict
v2beta2_object_metric_status_dict = v2beta2_object_metric_status_instance.to_dict()
# create an instance of V2beta2ObjectMetricStatus from a dict
v2beta2_object_metric_status_form_dict = v2beta2_object_metric_status.from_dict(v2beta2_object_metric_status_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)



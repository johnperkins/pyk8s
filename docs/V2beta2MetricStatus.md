# V2beta2MetricStatus

MetricStatus describes the last-read state of a single metric.

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**container_resource** | [**V2beta2ContainerResourceMetricStatus**](V2beta2ContainerResourceMetricStatus.md) |  | [optional] 
**external** | [**V2beta2ExternalMetricStatus**](V2beta2ExternalMetricStatus.md) |  | [optional] 
**object** | [**V2beta2ObjectMetricStatus**](V2beta2ObjectMetricStatus.md) |  | [optional] 
**pods** | [**V2beta2PodsMetricStatus**](V2beta2PodsMetricStatus.md) |  | [optional] 
**resource** | [**V2beta2ResourceMetricStatus**](V2beta2ResourceMetricStatus.md) |  | [optional] 
**type** | **str** | type is the type of metric source.  It will be one of \&quot;ContainerResource\&quot;, \&quot;External\&quot;, \&quot;Object\&quot;, \&quot;Pods\&quot; or \&quot;Resource\&quot;, each corresponds to a matching field in the object. Note: \&quot;ContainerResource\&quot; type is available on when the feature-gate HPAContainerMetrics is enabled | 

## Example

```python
from pyk8s.models.v2beta2_metric_status import V2beta2MetricStatus

# TODO update the JSON string below
json = "{}"
# create an instance of V2beta2MetricStatus from a JSON string
v2beta2_metric_status_instance = V2beta2MetricStatus.from_json(json)
# print the JSON string representation of the object
print V2beta2MetricStatus.to_json()

# convert the object into a dict
v2beta2_metric_status_dict = v2beta2_metric_status_instance.to_dict()
# create an instance of V2beta2MetricStatus from a dict
v2beta2_metric_status_form_dict = v2beta2_metric_status.from_dict(v2beta2_metric_status_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)



# V1beta2LimitedPriorityLevelConfiguration

LimitedPriorityLevelConfiguration specifies how to handle requests that are subject to limits. It addresses two issues:   - How are requests for this priority level limited?   - What should be done with requests that exceed the limit?

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**assured_concurrency_shares** | **int** | &#x60;assuredConcurrencyShares&#x60; (ACS) configures the execution limit, which is a limit on the number of requests of this priority level that may be exeucting at a given time.  ACS must be a positive number. The server&#39;s concurrency limit (SCL) is divided among the concurrency-controlled priority levels in proportion to their assured concurrency shares. This produces the assured concurrency value (ACV) --- the number of requests that may be executing at a time --- for each such priority level:              ACV(l) &#x3D; ceil( SCL * ACS(l) / ( sum[priority levels k] ACS(k) ) )  bigger numbers of ACS mean more reserved concurrent requests (at the expense of every other PL). This field has a default value of 30. | [optional] 
**limit_response** | [**V1beta2LimitResponse**](V1beta2LimitResponse.md) |  | [optional] 

## Example

```python
from pyk8s.models.v1beta2_limited_priority_level_configuration import V1beta2LimitedPriorityLevelConfiguration

# TODO update the JSON string below
json = "{}"
# create an instance of V1beta2LimitedPriorityLevelConfiguration from a JSON string
v1beta2_limited_priority_level_configuration_instance = V1beta2LimitedPriorityLevelConfiguration.from_json(json)
# print the JSON string representation of the object
print V1beta2LimitedPriorityLevelConfiguration.to_json()

# convert the object into a dict
v1beta2_limited_priority_level_configuration_dict = v1beta2_limited_priority_level_configuration_instance.to_dict()
# create an instance of V1beta2LimitedPriorityLevelConfiguration from a dict
v1beta2_limited_priority_level_configuration_form_dict = v1beta2_limited_priority_level_configuration.from_dict(v1beta2_limited_priority_level_configuration_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)



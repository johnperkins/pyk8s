# V2beta2ExternalMetricSource

ExternalMetricSource indicates how to scale on a metric not associated with any Kubernetes object (for example length of queue in cloud messaging service, or QPS from loadbalancer running outside of cluster).

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**metric** | [**V2beta2MetricIdentifier**](V2beta2MetricIdentifier.md) |  | 
**target** | [**V2beta2MetricTarget**](V2beta2MetricTarget.md) |  | 

## Example

```python
from pyk8s.models.v2beta2_external_metric_source import V2beta2ExternalMetricSource

# TODO update the JSON string below
json = "{}"
# create an instance of V2beta2ExternalMetricSource from a JSON string
v2beta2_external_metric_source_instance = V2beta2ExternalMetricSource.from_json(json)
# print the JSON string representation of the object
print V2beta2ExternalMetricSource.to_json()

# convert the object into a dict
v2beta2_external_metric_source_dict = v2beta2_external_metric_source_instance.to_dict()
# create an instance of V2beta2ExternalMetricSource from a dict
v2beta2_external_metric_source_form_dict = v2beta2_external_metric_source.from_dict(v2beta2_external_metric_source_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)



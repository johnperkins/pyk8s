# V2beta2ObjectMetricSource

ObjectMetricSource indicates how to scale on a metric describing a kubernetes object (for example, hits-per-second on an Ingress object).

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**described_object** | [**V2beta2CrossVersionObjectReference**](V2beta2CrossVersionObjectReference.md) |  | 
**metric** | [**V2beta2MetricIdentifier**](V2beta2MetricIdentifier.md) |  | 
**target** | [**V2beta2MetricTarget**](V2beta2MetricTarget.md) |  | 

## Example

```python
from pyk8s.models.v2beta2_object_metric_source import V2beta2ObjectMetricSource

# TODO update the JSON string below
json = "{}"
# create an instance of V2beta2ObjectMetricSource from a JSON string
v2beta2_object_metric_source_instance = V2beta2ObjectMetricSource.from_json(json)
# print the JSON string representation of the object
print V2beta2ObjectMetricSource.to_json()

# convert the object into a dict
v2beta2_object_metric_source_dict = v2beta2_object_metric_source_instance.to_dict()
# create an instance of V2beta2ObjectMetricSource from a dict
v2beta2_object_metric_source_form_dict = v2beta2_object_metric_source.from_dict(v2beta2_object_metric_source_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)



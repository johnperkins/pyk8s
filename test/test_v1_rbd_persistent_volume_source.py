# coding: utf-8

"""
    Kubernetes

    No description provided (generated by Openapi Generator https://github.com/openapitools/openapi-generator)  # noqa: E501

    The version of the OpenAPI document: release-1.25
    Generated by: https://openapi-generator.tech
"""


from __future__ import absolute_import

import unittest
import datetime

import pyk8s
from pyk8s.models.v1_rbd_persistent_volume_source import V1RBDPersistentVolumeSource  # noqa: E501
from pyk8s.rest import ApiException

class TestV1RBDPersistentVolumeSource(unittest.TestCase):
    """V1RBDPersistentVolumeSource unit test stubs"""

    def setUp(self):
        pass

    def tearDown(self):
        pass

    def make_instance(self, include_optional):
        """Test V1RBDPersistentVolumeSource
            include_option is a boolean, when False only required
            params are included, when True both required and
            optional params are included """
        # uncomment below to create an instance of `V1RBDPersistentVolumeSource`
        """
        model = pyk8s.models.v1_rbd_persistent_volume_source.V1RBDPersistentVolumeSource()  # noqa: E501
        if include_optional :
            return V1RBDPersistentVolumeSource(
                fs_type = '', 
                image = '', 
                keyring = '', 
                monitors = [
                    ''
                    ], 
                pool = '', 
                read_only = True, 
                secret_ref = pyk8s.models.v1/secret_reference.v1.SecretReference(
                    name = '', 
                    namespace = '', ), 
                user = ''
            )
        else :
            return V1RBDPersistentVolumeSource(
                image = '',
                monitors = [
                    ''
                    ],
        )
        """

    def testV1RBDPersistentVolumeSource(self):
        """Test V1RBDPersistentVolumeSource"""
        # inst_req_only = self.make_instance(include_optional=False)
        # inst_req_and_optional = self.make_instance(include_optional=True)

if __name__ == '__main__':
    unittest.main()

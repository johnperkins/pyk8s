# coding: utf-8

"""
    Kubernetes

    No description provided (generated by Openapi Generator https://github.com/openapitools/openapi-generator)  # noqa: E501

    The version of the OpenAPI document: release-1.25
    Generated by: https://openapi-generator.tech
"""


from __future__ import absolute_import

import unittest
import datetime

import pyk8s
from pyk8s.models.v1_job_template_spec import V1JobTemplateSpec  # noqa: E501
from pyk8s.rest import ApiException

class TestV1JobTemplateSpec(unittest.TestCase):
    """V1JobTemplateSpec unit test stubs"""

    def setUp(self):
        pass

    def tearDown(self):
        pass

    def make_instance(self, include_optional):
        """Test V1JobTemplateSpec
            include_option is a boolean, when False only required
            params are included, when True both required and
            optional params are included """
        # uncomment below to create an instance of `V1JobTemplateSpec`
        """
        model = pyk8s.models.v1_job_template_spec.V1JobTemplateSpec()  # noqa: E501
        if include_optional :
            return V1JobTemplateSpec(
                metadata = pyk8s.models.v1/object_meta.v1.ObjectMeta(
                    annotations = {
                        'key' : ''
                        }, 
                    creation_timestamp = datetime.datetime.strptime('2013-10-20 19:20:30.00', '%Y-%m-%d %H:%M:%S.%f'), 
                    deletion_grace_period_seconds = 56, 
                    deletion_timestamp = datetime.datetime.strptime('2013-10-20 19:20:30.00', '%Y-%m-%d %H:%M:%S.%f'), 
                    finalizers = [
                        ''
                        ], 
                    generate_name = '', 
                    generation = 56, 
                    labels = {
                        'key' : ''
                        }, 
                    managed_fields = [
                        pyk8s.models.v1/managed_fields_entry.v1.ManagedFieldsEntry(
                            api_version = '', 
                            fields_type = '', 
                            fields_v1 = pyk8s.models.fields_v1.fieldsV1(), 
                            manager = '', 
                            operation = '', 
                            subresource = '', 
                            time = datetime.datetime.strptime('2013-10-20 19:20:30.00', '%Y-%m-%d %H:%M:%S.%f'), )
                        ], 
                    name = '', 
                    namespace = '', 
                    owner_references = [
                        pyk8s.models.v1/owner_reference.v1.OwnerReference(
                            api_version = '', 
                            block_owner_deletion = True, 
                            controller = True, 
                            kind = '', 
                            name = '', 
                            uid = '', )
                        ], 
                    resource_version = '', 
                    self_link = '', 
                    uid = '', ), 
                spec = pyk8s.models.v1/job_spec.v1.JobSpec(
                    active_deadline_seconds = 56, 
                    backoff_limit = 56, 
                    completion_mode = '', 
                    completions = 56, 
                    manual_selector = True, 
                    parallelism = 56, 
                    pod_failure_policy = pyk8s.models.v1/pod_failure_policy.v1.PodFailurePolicy(
                        rules = [
                            pyk8s.models.v1/pod_failure_policy_rule.v1.PodFailurePolicyRule(
                                action = '', 
                                on_exit_codes = pyk8s.models.v1/pod_failure_policy_on_exit_codes_requirement.v1.PodFailurePolicyOnExitCodesRequirement(
                                    container_name = '', 
                                    operator = '', 
                                    values = [
                                        56
                                        ], ), 
                                on_pod_conditions = [
                                    pyk8s.models.v1/pod_failure_policy_on_pod_conditions_pattern.v1.PodFailurePolicyOnPodConditionsPattern(
                                        status = '', 
                                        type = '', )
                                    ], )
                            ], ), 
                    selector = pyk8s.models.v1/label_selector.v1.LabelSelector(
                        match_expressions = [
                            pyk8s.models.v1/label_selector_requirement.v1.LabelSelectorRequirement(
                                key = '', 
                                operator = '', )
                            ], 
                        match_labels = {
                            'key' : ''
                            }, ), 
                    suspend = True, 
                    template = pyk8s.models.v1/pod_template_spec.v1.PodTemplateSpec(
                        metadata = pyk8s.models.v1/object_meta.v1.ObjectMeta(
                            annotations = {
                                'key' : ''
                                }, 
                            creation_timestamp = datetime.datetime.strptime('2013-10-20 19:20:30.00', '%Y-%m-%d %H:%M:%S.%f'), 
                            deletion_grace_period_seconds = 56, 
                            deletion_timestamp = datetime.datetime.strptime('2013-10-20 19:20:30.00', '%Y-%m-%d %H:%M:%S.%f'), 
                            finalizers = [
                                ''
                                ], 
                            generate_name = '', 
                            generation = 56, 
                            labels = {
                                'key' : ''
                                }, 
                            managed_fields = [
                                pyk8s.models.v1/managed_fields_entry.v1.ManagedFieldsEntry(
                                    api_version = '', 
                                    fields_type = '', 
                                    fields_v1 = pyk8s.models.fields_v1.fieldsV1(), 
                                    manager = '', 
                                    operation = '', 
                                    subresource = '', 
                                    time = datetime.datetime.strptime('2013-10-20 19:20:30.00', '%Y-%m-%d %H:%M:%S.%f'), )
                                ], 
                            name = '', 
                            namespace = '', 
                            owner_references = [
                                pyk8s.models.v1/owner_reference.v1.OwnerReference(
                                    api_version = '', 
                                    block_owner_deletion = True, 
                                    controller = True, 
                                    kind = '', 
                                    name = '', 
                                    uid = '', )
                                ], 
                            resource_version = '', 
                            self_link = '', 
                            uid = '', ), ), 
                    ttl_seconds_after_finished = 56, )
            )
        else :
            return V1JobTemplateSpec(
        )
        """

    def testV1JobTemplateSpec(self):
        """Test V1JobTemplateSpec"""
        # inst_req_only = self.make_instance(include_optional=False)
        # inst_req_and_optional = self.make_instance(include_optional=True)

if __name__ == '__main__':
    unittest.main()

# coding: utf-8

"""
    Kubernetes

    No description provided (generated by Openapi Generator https://github.com/openapitools/openapi-generator)  # noqa: E501

    The version of the OpenAPI document: release-1.25
    Generated by: https://openapi-generator.tech
"""


from __future__ import absolute_import

import unittest
import datetime

import pyk8s
from pyk8s.models.v1_node_system_info import V1NodeSystemInfo  # noqa: E501
from pyk8s.rest import ApiException

class TestV1NodeSystemInfo(unittest.TestCase):
    """V1NodeSystemInfo unit test stubs"""

    def setUp(self):
        pass

    def tearDown(self):
        pass

    def make_instance(self, include_optional):
        """Test V1NodeSystemInfo
            include_option is a boolean, when False only required
            params are included, when True both required and
            optional params are included """
        # uncomment below to create an instance of `V1NodeSystemInfo`
        """
        model = pyk8s.models.v1_node_system_info.V1NodeSystemInfo()  # noqa: E501
        if include_optional :
            return V1NodeSystemInfo(
                architecture = '', 
                boot_id = '', 
                container_runtime_version = '', 
                kernel_version = '', 
                kube_proxy_version = '', 
                kubelet_version = '', 
                machine_id = '', 
                operating_system = '', 
                os_image = '', 
                system_uuid = ''
            )
        else :
            return V1NodeSystemInfo(
                architecture = '',
                boot_id = '',
                container_runtime_version = '',
                kernel_version = '',
                kube_proxy_version = '',
                kubelet_version = '',
                machine_id = '',
                operating_system = '',
                os_image = '',
                system_uuid = '',
        )
        """

    def testV1NodeSystemInfo(self):
        """Test V1NodeSystemInfo"""
        # inst_req_only = self.make_instance(include_optional=False)
        # inst_req_and_optional = self.make_instance(include_optional=True)

if __name__ == '__main__':
    unittest.main()

# coding: utf-8

"""
    Kubernetes

    No description provided (generated by Openapi Generator https://github.com/openapitools/openapi-generator)  # noqa: E501

    The version of the OpenAPI document: release-1.25
    Generated by: https://openapi-generator.tech
"""


from __future__ import absolute_import

import unittest
import datetime

import pyk8s
from pyk8s.models.v1_endpoint_address import V1EndpointAddress  # noqa: E501
from pyk8s.rest import ApiException

class TestV1EndpointAddress(unittest.TestCase):
    """V1EndpointAddress unit test stubs"""

    def setUp(self):
        pass

    def tearDown(self):
        pass

    def make_instance(self, include_optional):
        """Test V1EndpointAddress
            include_option is a boolean, when False only required
            params are included, when True both required and
            optional params are included """
        # uncomment below to create an instance of `V1EndpointAddress`
        """
        model = pyk8s.models.v1_endpoint_address.V1EndpointAddress()  # noqa: E501
        if include_optional :
            return V1EndpointAddress(
                hostname = '', 
                ip = '', 
                node_name = '', 
                target_ref = pyk8s.models.v1/object_reference.v1.ObjectReference(
                    api_version = '', 
                    field_path = '', 
                    kind = '', 
                    name = '', 
                    namespace = '', 
                    resource_version = '', 
                    uid = '', )
            )
        else :
            return V1EndpointAddress(
                ip = '',
        )
        """

    def testV1EndpointAddress(self):
        """Test V1EndpointAddress"""
        # inst_req_only = self.make_instance(include_optional=False)
        # inst_req_and_optional = self.make_instance(include_optional=True)

if __name__ == '__main__':
    unittest.main()

# coding: utf-8

"""
    Kubernetes

    No description provided (generated by Openapi Generator https://github.com/openapitools/openapi-generator)  # noqa: E501

    The version of the OpenAPI document: release-1.25
    Generated by: https://openapi-generator.tech
"""


from __future__ import absolute_import

import unittest
import datetime

import pyk8s
from pyk8s.models.v1_api_service_list import V1APIServiceList  # noqa: E501
from pyk8s.rest import ApiException

class TestV1APIServiceList(unittest.TestCase):
    """V1APIServiceList unit test stubs"""

    def setUp(self):
        pass

    def tearDown(self):
        pass

    def make_instance(self, include_optional):
        """Test V1APIServiceList
            include_option is a boolean, when False only required
            params are included, when True both required and
            optional params are included """
        # uncomment below to create an instance of `V1APIServiceList`
        """
        model = pyk8s.models.v1_api_service_list.V1APIServiceList()  # noqa: E501
        if include_optional :
            return V1APIServiceList(
                api_version = '', 
                items = [
                    pyk8s.models.v1/api_service.v1.APIService(
                        api_version = '', 
                        kind = '', 
                        metadata = pyk8s.models.v1/object_meta.v1.ObjectMeta(
                            annotations = {
                                'key' : ''
                                }, 
                            creation_timestamp = datetime.datetime.strptime('2013-10-20 19:20:30.00', '%Y-%m-%d %H:%M:%S.%f'), 
                            deletion_grace_period_seconds = 56, 
                            deletion_timestamp = datetime.datetime.strptime('2013-10-20 19:20:30.00', '%Y-%m-%d %H:%M:%S.%f'), 
                            finalizers = [
                                ''
                                ], 
                            generate_name = '', 
                            generation = 56, 
                            labels = {
                                'key' : ''
                                }, 
                            managed_fields = [
                                pyk8s.models.v1/managed_fields_entry.v1.ManagedFieldsEntry(
                                    api_version = '', 
                                    fields_type = '', 
                                    fields_v1 = pyk8s.models.fields_v1.fieldsV1(), 
                                    manager = '', 
                                    operation = '', 
                                    subresource = '', 
                                    time = datetime.datetime.strptime('2013-10-20 19:20:30.00', '%Y-%m-%d %H:%M:%S.%f'), )
                                ], 
                            name = '', 
                            namespace = '', 
                            owner_references = [
                                pyk8s.models.v1/owner_reference.v1.OwnerReference(
                                    api_version = '', 
                                    block_owner_deletion = True, 
                                    controller = True, 
                                    kind = '', 
                                    name = '', 
                                    uid = '', )
                                ], 
                            resource_version = '', 
                            self_link = '', 
                            uid = '', ), 
                        spec = pyk8s.models.v1/api_service_spec.v1.APIServiceSpec(
                            ca_bundle = 'YQ==', 
                            group = '', 
                            group_priority_minimum = 56, 
                            insecure_skip_tls_verify = True, 
                            service = pyk8s.models.apiregistration/v1/service_reference.apiregistration.v1.ServiceReference(
                                name = '', 
                                namespace = '', 
                                port = 56, ), 
                            version = '', 
                            version_priority = 56, ), 
                        status = pyk8s.models.v1/api_service_status.v1.APIServiceStatus(
                            conditions = [
                                pyk8s.models.v1/api_service_condition.v1.APIServiceCondition(
                                    last_transition_time = datetime.datetime.strptime('2013-10-20 19:20:30.00', '%Y-%m-%d %H:%M:%S.%f'), 
                                    message = '', 
                                    reason = '', 
                                    status = '', 
                                    type = '', )
                                ], ), )
                    ], 
                kind = '', 
                metadata = pyk8s.models.v1/list_meta.v1.ListMeta(
                    continue = '', 
                    remaining_item_count = 56, 
                    resource_version = '', 
                    self_link = '', )
            )
        else :
            return V1APIServiceList(
                items = [
                    pyk8s.models.v1/api_service.v1.APIService(
                        api_version = '', 
                        kind = '', 
                        metadata = pyk8s.models.v1/object_meta.v1.ObjectMeta(
                            annotations = {
                                'key' : ''
                                }, 
                            creation_timestamp = datetime.datetime.strptime('2013-10-20 19:20:30.00', '%Y-%m-%d %H:%M:%S.%f'), 
                            deletion_grace_period_seconds = 56, 
                            deletion_timestamp = datetime.datetime.strptime('2013-10-20 19:20:30.00', '%Y-%m-%d %H:%M:%S.%f'), 
                            finalizers = [
                                ''
                                ], 
                            generate_name = '', 
                            generation = 56, 
                            labels = {
                                'key' : ''
                                }, 
                            managed_fields = [
                                pyk8s.models.v1/managed_fields_entry.v1.ManagedFieldsEntry(
                                    api_version = '', 
                                    fields_type = '', 
                                    fields_v1 = pyk8s.models.fields_v1.fieldsV1(), 
                                    manager = '', 
                                    operation = '', 
                                    subresource = '', 
                                    time = datetime.datetime.strptime('2013-10-20 19:20:30.00', '%Y-%m-%d %H:%M:%S.%f'), )
                                ], 
                            name = '', 
                            namespace = '', 
                            owner_references = [
                                pyk8s.models.v1/owner_reference.v1.OwnerReference(
                                    api_version = '', 
                                    block_owner_deletion = True, 
                                    controller = True, 
                                    kind = '', 
                                    name = '', 
                                    uid = '', )
                                ], 
                            resource_version = '', 
                            self_link = '', 
                            uid = '', ), 
                        spec = pyk8s.models.v1/api_service_spec.v1.APIServiceSpec(
                            ca_bundle = 'YQ==', 
                            group = '', 
                            group_priority_minimum = 56, 
                            insecure_skip_tls_verify = True, 
                            service = pyk8s.models.apiregistration/v1/service_reference.apiregistration.v1.ServiceReference(
                                name = '', 
                                namespace = '', 
                                port = 56, ), 
                            version = '', 
                            version_priority = 56, ), 
                        status = pyk8s.models.v1/api_service_status.v1.APIServiceStatus(
                            conditions = [
                                pyk8s.models.v1/api_service_condition.v1.APIServiceCondition(
                                    last_transition_time = datetime.datetime.strptime('2013-10-20 19:20:30.00', '%Y-%m-%d %H:%M:%S.%f'), 
                                    message = '', 
                                    reason = '', 
                                    status = '', 
                                    type = '', )
                                ], ), )
                    ],
        )
        """

    def testV1APIServiceList(self):
        """Test V1APIServiceList"""
        # inst_req_only = self.make_instance(include_optional=False)
        # inst_req_and_optional = self.make_instance(include_optional=True)

if __name__ == '__main__':
    unittest.main()

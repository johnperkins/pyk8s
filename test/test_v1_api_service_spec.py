# coding: utf-8

"""
    Kubernetes

    No description provided (generated by Openapi Generator https://github.com/openapitools/openapi-generator)  # noqa: E501

    The version of the OpenAPI document: release-1.25
    Generated by: https://openapi-generator.tech
"""


from __future__ import absolute_import

import unittest
import datetime

import pyk8s
from pyk8s.models.v1_api_service_spec import V1APIServiceSpec  # noqa: E501
from pyk8s.rest import ApiException

class TestV1APIServiceSpec(unittest.TestCase):
    """V1APIServiceSpec unit test stubs"""

    def setUp(self):
        pass

    def tearDown(self):
        pass

    def make_instance(self, include_optional):
        """Test V1APIServiceSpec
            include_option is a boolean, when False only required
            params are included, when True both required and
            optional params are included """
        # uncomment below to create an instance of `V1APIServiceSpec`
        """
        model = pyk8s.models.v1_api_service_spec.V1APIServiceSpec()  # noqa: E501
        if include_optional :
            return V1APIServiceSpec(
                ca_bundle = 'YQ==', 
                group = '', 
                group_priority_minimum = 56, 
                insecure_skip_tls_verify = True, 
                service = pyk8s.models.apiregistration/v1/service_reference.apiregistration.v1.ServiceReference(
                    name = '', 
                    namespace = '', 
                    port = 56, ), 
                version = '', 
                version_priority = 56
            )
        else :
            return V1APIServiceSpec(
                group_priority_minimum = 56,
                version_priority = 56,
        )
        """

    def testV1APIServiceSpec(self):
        """Test V1APIServiceSpec"""
        # inst_req_only = self.make_instance(include_optional=False)
        # inst_req_and_optional = self.make_instance(include_optional=True)

if __name__ == '__main__':
    unittest.main()

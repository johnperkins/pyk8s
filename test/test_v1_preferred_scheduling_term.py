# coding: utf-8

"""
    Kubernetes

    No description provided (generated by Openapi Generator https://github.com/openapitools/openapi-generator)  # noqa: E501

    The version of the OpenAPI document: release-1.25
    Generated by: https://openapi-generator.tech
"""


from __future__ import absolute_import

import unittest
import datetime

import pyk8s
from pyk8s.models.v1_preferred_scheduling_term import V1PreferredSchedulingTerm  # noqa: E501
from pyk8s.rest import ApiException

class TestV1PreferredSchedulingTerm(unittest.TestCase):
    """V1PreferredSchedulingTerm unit test stubs"""

    def setUp(self):
        pass

    def tearDown(self):
        pass

    def make_instance(self, include_optional):
        """Test V1PreferredSchedulingTerm
            include_option is a boolean, when False only required
            params are included, when True both required and
            optional params are included """
        # uncomment below to create an instance of `V1PreferredSchedulingTerm`
        """
        model = pyk8s.models.v1_preferred_scheduling_term.V1PreferredSchedulingTerm()  # noqa: E501
        if include_optional :
            return V1PreferredSchedulingTerm(
                preference = pyk8s.models.v1/node_selector_term.v1.NodeSelectorTerm(
                    match_expressions = [
                        pyk8s.models.v1/node_selector_requirement.v1.NodeSelectorRequirement(
                            key = '', 
                            operator = '', 
                            values = [
                                ''
                                ], )
                        ], 
                    match_fields = [
                        pyk8s.models.v1/node_selector_requirement.v1.NodeSelectorRequirement(
                            key = '', 
                            operator = '', )
                        ], ), 
                weight = 56
            )
        else :
            return V1PreferredSchedulingTerm(
                preference = pyk8s.models.v1/node_selector_term.v1.NodeSelectorTerm(
                    match_expressions = [
                        pyk8s.models.v1/node_selector_requirement.v1.NodeSelectorRequirement(
                            key = '', 
                            operator = '', 
                            values = [
                                ''
                                ], )
                        ], 
                    match_fields = [
                        pyk8s.models.v1/node_selector_requirement.v1.NodeSelectorRequirement(
                            key = '', 
                            operator = '', )
                        ], ),
                weight = 56,
        )
        """

    def testV1PreferredSchedulingTerm(self):
        """Test V1PreferredSchedulingTerm"""
        # inst_req_only = self.make_instance(include_optional=False)
        # inst_req_and_optional = self.make_instance(include_optional=True)

if __name__ == '__main__':
    unittest.main()

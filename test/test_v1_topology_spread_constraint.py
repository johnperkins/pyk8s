# coding: utf-8

"""
    Kubernetes

    No description provided (generated by Openapi Generator https://github.com/openapitools/openapi-generator)  # noqa: E501

    The version of the OpenAPI document: release-1.25
    Generated by: https://openapi-generator.tech
"""


from __future__ import absolute_import

import unittest
import datetime

import pyk8s
from pyk8s.models.v1_topology_spread_constraint import V1TopologySpreadConstraint  # noqa: E501
from pyk8s.rest import ApiException

class TestV1TopologySpreadConstraint(unittest.TestCase):
    """V1TopologySpreadConstraint unit test stubs"""

    def setUp(self):
        pass

    def tearDown(self):
        pass

    def make_instance(self, include_optional):
        """Test V1TopologySpreadConstraint
            include_option is a boolean, when False only required
            params are included, when True both required and
            optional params are included """
        # uncomment below to create an instance of `V1TopologySpreadConstraint`
        """
        model = pyk8s.models.v1_topology_spread_constraint.V1TopologySpreadConstraint()  # noqa: E501
        if include_optional :
            return V1TopologySpreadConstraint(
                label_selector = pyk8s.models.v1/label_selector.v1.LabelSelector(
                    match_expressions = [
                        pyk8s.models.v1/label_selector_requirement.v1.LabelSelectorRequirement(
                            key = '', 
                            operator = '', 
                            values = [
                                ''
                                ], )
                        ], 
                    match_labels = {
                        'key' : ''
                        }, ), 
                match_label_keys = [
                    ''
                    ], 
                max_skew = 56, 
                min_domains = 56, 
                node_affinity_policy = '', 
                node_taints_policy = '', 
                topology_key = '', 
                when_unsatisfiable = ''
            )
        else :
            return V1TopologySpreadConstraint(
                max_skew = 56,
                topology_key = '',
                when_unsatisfiable = '',
        )
        """

    def testV1TopologySpreadConstraint(self):
        """Test V1TopologySpreadConstraint"""
        # inst_req_only = self.make_instance(include_optional=False)
        # inst_req_and_optional = self.make_instance(include_optional=True)

if __name__ == '__main__':
    unittest.main()

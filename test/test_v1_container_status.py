# coding: utf-8

"""
    Kubernetes

    No description provided (generated by Openapi Generator https://github.com/openapitools/openapi-generator)  # noqa: E501

    The version of the OpenAPI document: release-1.25
    Generated by: https://openapi-generator.tech
"""


from __future__ import absolute_import

import unittest
import datetime

import pyk8s
from pyk8s.models.v1_container_status import V1ContainerStatus  # noqa: E501
from pyk8s.rest import ApiException

class TestV1ContainerStatus(unittest.TestCase):
    """V1ContainerStatus unit test stubs"""

    def setUp(self):
        pass

    def tearDown(self):
        pass

    def make_instance(self, include_optional):
        """Test V1ContainerStatus
            include_option is a boolean, when False only required
            params are included, when True both required and
            optional params are included """
        # uncomment below to create an instance of `V1ContainerStatus`
        """
        model = pyk8s.models.v1_container_status.V1ContainerStatus()  # noqa: E501
        if include_optional :
            return V1ContainerStatus(
                container_id = '', 
                image = '', 
                image_id = '', 
                last_state = pyk8s.models.v1/container_state.v1.ContainerState(
                    running = pyk8s.models.v1/container_state_running.v1.ContainerStateRunning(
                        started_at = datetime.datetime.strptime('2013-10-20 19:20:30.00', '%Y-%m-%d %H:%M:%S.%f'), ), 
                    terminated = pyk8s.models.v1/container_state_terminated.v1.ContainerStateTerminated(
                        container_id = '', 
                        exit_code = 56, 
                        finished_at = datetime.datetime.strptime('2013-10-20 19:20:30.00', '%Y-%m-%d %H:%M:%S.%f'), 
                        message = '', 
                        reason = '', 
                        signal = 56, 
                        started_at = datetime.datetime.strptime('2013-10-20 19:20:30.00', '%Y-%m-%d %H:%M:%S.%f'), ), 
                    waiting = pyk8s.models.v1/container_state_waiting.v1.ContainerStateWaiting(
                        message = '', 
                        reason = '', ), ), 
                name = '', 
                ready = True, 
                restart_count = 56, 
                started = True, 
                state = pyk8s.models.v1/container_state.v1.ContainerState(
                    running = pyk8s.models.v1/container_state_running.v1.ContainerStateRunning(
                        started_at = datetime.datetime.strptime('2013-10-20 19:20:30.00', '%Y-%m-%d %H:%M:%S.%f'), ), 
                    terminated = pyk8s.models.v1/container_state_terminated.v1.ContainerStateTerminated(
                        container_id = '', 
                        exit_code = 56, 
                        finished_at = datetime.datetime.strptime('2013-10-20 19:20:30.00', '%Y-%m-%d %H:%M:%S.%f'), 
                        message = '', 
                        reason = '', 
                        signal = 56, 
                        started_at = datetime.datetime.strptime('2013-10-20 19:20:30.00', '%Y-%m-%d %H:%M:%S.%f'), ), 
                    waiting = pyk8s.models.v1/container_state_waiting.v1.ContainerStateWaiting(
                        message = '', 
                        reason = '', ), )
            )
        else :
            return V1ContainerStatus(
                image = '',
                image_id = '',
                name = '',
                ready = True,
                restart_count = 56,
        )
        """

    def testV1ContainerStatus(self):
        """Test V1ContainerStatus"""
        # inst_req_only = self.make_instance(include_optional=False)
        # inst_req_and_optional = self.make_instance(include_optional=True)

if __name__ == '__main__':
    unittest.main()

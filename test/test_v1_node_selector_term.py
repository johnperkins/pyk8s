# coding: utf-8

"""
    Kubernetes

    No description provided (generated by Openapi Generator https://github.com/openapitools/openapi-generator)  # noqa: E501

    The version of the OpenAPI document: release-1.25
    Generated by: https://openapi-generator.tech
"""


from __future__ import absolute_import

import unittest
import datetime

import pyk8s
from pyk8s.models.v1_node_selector_term import V1NodeSelectorTerm  # noqa: E501
from pyk8s.rest import ApiException

class TestV1NodeSelectorTerm(unittest.TestCase):
    """V1NodeSelectorTerm unit test stubs"""

    def setUp(self):
        pass

    def tearDown(self):
        pass

    def make_instance(self, include_optional):
        """Test V1NodeSelectorTerm
            include_option is a boolean, when False only required
            params are included, when True both required and
            optional params are included """
        # uncomment below to create an instance of `V1NodeSelectorTerm`
        """
        model = pyk8s.models.v1_node_selector_term.V1NodeSelectorTerm()  # noqa: E501
        if include_optional :
            return V1NodeSelectorTerm(
                match_expressions = [
                    pyk8s.models.v1/node_selector_requirement.v1.NodeSelectorRequirement(
                        key = '', 
                        operator = '', 
                        values = [
                            ''
                            ], )
                    ], 
                match_fields = [
                    pyk8s.models.v1/node_selector_requirement.v1.NodeSelectorRequirement(
                        key = '', 
                        operator = '', 
                        values = [
                            ''
                            ], )
                    ]
            )
        else :
            return V1NodeSelectorTerm(
        )
        """

    def testV1NodeSelectorTerm(self):
        """Test V1NodeSelectorTerm"""
        # inst_req_only = self.make_instance(include_optional=False)
        # inst_req_and_optional = self.make_instance(include_optional=True)

if __name__ == '__main__':
    unittest.main()

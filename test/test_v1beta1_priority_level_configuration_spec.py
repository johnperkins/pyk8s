# coding: utf-8

"""
    Kubernetes

    No description provided (generated by Openapi Generator https://github.com/openapitools/openapi-generator)  # noqa: E501

    The version of the OpenAPI document: release-1.25
    Generated by: https://openapi-generator.tech
"""


from __future__ import absolute_import

import unittest
import datetime

import pyk8s
from pyk8s.models.v1beta1_priority_level_configuration_spec import V1beta1PriorityLevelConfigurationSpec  # noqa: E501
from pyk8s.rest import ApiException

class TestV1beta1PriorityLevelConfigurationSpec(unittest.TestCase):
    """V1beta1PriorityLevelConfigurationSpec unit test stubs"""

    def setUp(self):
        pass

    def tearDown(self):
        pass

    def make_instance(self, include_optional):
        """Test V1beta1PriorityLevelConfigurationSpec
            include_option is a boolean, when False only required
            params are included, when True both required and
            optional params are included """
        # uncomment below to create an instance of `V1beta1PriorityLevelConfigurationSpec`
        """
        model = pyk8s.models.v1beta1_priority_level_configuration_spec.V1beta1PriorityLevelConfigurationSpec()  # noqa: E501
        if include_optional :
            return V1beta1PriorityLevelConfigurationSpec(
                limited = pyk8s.models.v1beta1/limited_priority_level_configuration.v1beta1.LimitedPriorityLevelConfiguration(
                    assured_concurrency_shares = 56, 
                    limit_response = pyk8s.models.v1beta1/limit_response.v1beta1.LimitResponse(
                        queuing = pyk8s.models.v1beta1/queuing_configuration.v1beta1.QueuingConfiguration(
                            hand_size = 56, 
                            queue_length_limit = 56, 
                            queues = 56, ), 
                        type = '', ), ), 
                type = ''
            )
        else :
            return V1beta1PriorityLevelConfigurationSpec(
                type = '',
        )
        """

    def testV1beta1PriorityLevelConfigurationSpec(self):
        """Test V1beta1PriorityLevelConfigurationSpec"""
        # inst_req_only = self.make_instance(include_optional=False)
        # inst_req_and_optional = self.make_instance(include_optional=True)

if __name__ == '__main__':
    unittest.main()

# coding: utf-8

"""
    Kubernetes

    No description provided (generated by Openapi Generator https://github.com/openapitools/openapi-generator)  # noqa: E501

    The version of the OpenAPI document: release-1.25
    Generated by: https://openapi-generator.tech
"""


from __future__ import absolute_import

import unittest
import datetime

import pyk8s
from pyk8s.models.v1beta1_user_subject import V1beta1UserSubject  # noqa: E501
from pyk8s.rest import ApiException

class TestV1beta1UserSubject(unittest.TestCase):
    """V1beta1UserSubject unit test stubs"""

    def setUp(self):
        pass

    def tearDown(self):
        pass

    def make_instance(self, include_optional):
        """Test V1beta1UserSubject
            include_option is a boolean, when False only required
            params are included, when True both required and
            optional params are included """
        # uncomment below to create an instance of `V1beta1UserSubject`
        """
        model = pyk8s.models.v1beta1_user_subject.V1beta1UserSubject()  # noqa: E501
        if include_optional :
            return V1beta1UserSubject(
                name = ''
            )
        else :
            return V1beta1UserSubject(
                name = '',
        )
        """

    def testV1beta1UserSubject(self):
        """Test V1beta1UserSubject"""
        # inst_req_only = self.make_instance(include_optional=False)
        # inst_req_and_optional = self.make_instance(include_optional=True)

if __name__ == '__main__':
    unittest.main()

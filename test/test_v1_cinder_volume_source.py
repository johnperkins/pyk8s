# coding: utf-8

"""
    Kubernetes

    No description provided (generated by Openapi Generator https://github.com/openapitools/openapi-generator)  # noqa: E501

    The version of the OpenAPI document: release-1.25
    Generated by: https://openapi-generator.tech
"""


from __future__ import absolute_import

import unittest
import datetime

import pyk8s
from pyk8s.models.v1_cinder_volume_source import V1CinderVolumeSource  # noqa: E501
from pyk8s.rest import ApiException

class TestV1CinderVolumeSource(unittest.TestCase):
    """V1CinderVolumeSource unit test stubs"""

    def setUp(self):
        pass

    def tearDown(self):
        pass

    def make_instance(self, include_optional):
        """Test V1CinderVolumeSource
            include_option is a boolean, when False only required
            params are included, when True both required and
            optional params are included """
        # uncomment below to create an instance of `V1CinderVolumeSource`
        """
        model = pyk8s.models.v1_cinder_volume_source.V1CinderVolumeSource()  # noqa: E501
        if include_optional :
            return V1CinderVolumeSource(
                fs_type = '', 
                read_only = True, 
                secret_ref = pyk8s.models.v1/local_object_reference.v1.LocalObjectReference(
                    name = '', ), 
                volume_id = ''
            )
        else :
            return V1CinderVolumeSource(
                volume_id = '',
        )
        """

    def testV1CinderVolumeSource(self):
        """Test V1CinderVolumeSource"""
        # inst_req_only = self.make_instance(include_optional=False)
        # inst_req_and_optional = self.make_instance(include_optional=True)

if __name__ == '__main__':
    unittest.main()

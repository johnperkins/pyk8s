# coding: utf-8

"""
    Kubernetes

    No description provided (generated by Openapi Generator https://github.com/openapitools/openapi-generator)  # noqa: E501

    The version of the OpenAPI document: release-1.25
    Generated by: https://openapi-generator.tech
"""


from __future__ import absolute_import

import unittest
import datetime

import pyk8s
from pyk8s.models.v1_webhook_conversion import V1WebhookConversion  # noqa: E501
from pyk8s.rest import ApiException

class TestV1WebhookConversion(unittest.TestCase):
    """V1WebhookConversion unit test stubs"""

    def setUp(self):
        pass

    def tearDown(self):
        pass

    def make_instance(self, include_optional):
        """Test V1WebhookConversion
            include_option is a boolean, when False only required
            params are included, when True both required and
            optional params are included """
        # uncomment below to create an instance of `V1WebhookConversion`
        """
        model = pyk8s.models.v1_webhook_conversion.V1WebhookConversion()  # noqa: E501
        if include_optional :
            return V1WebhookConversion(
                client_config = pyk8s.models.apiextensions/v1/webhook_client_config.apiextensions.v1.WebhookClientConfig(
                    ca_bundle = 'YQ==', 
                    service = pyk8s.models.apiextensions/v1/service_reference.apiextensions.v1.ServiceReference(
                        name = '', 
                        namespace = '', 
                        path = '', 
                        port = 56, ), 
                    url = '', ), 
                conversion_review_versions = [
                    ''
                    ]
            )
        else :
            return V1WebhookConversion(
                conversion_review_versions = [
                    ''
                    ],
        )
        """

    def testV1WebhookConversion(self):
        """Test V1WebhookConversion"""
        # inst_req_only = self.make_instance(include_optional=False)
        # inst_req_and_optional = self.make_instance(include_optional=True)

if __name__ == '__main__':
    unittest.main()

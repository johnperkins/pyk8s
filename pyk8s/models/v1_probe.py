# coding: utf-8

"""
    Kubernetes

    No description provided (generated by Openapi Generator https://github.com/openapitools/openapi-generator)  # noqa: E501

    The version of the OpenAPI document: release-1.25
    Generated by: https://openapi-generator.tech
"""


from __future__ import annotations
from inspect import getfullargspec
import pprint
import re  # noqa: F401
import json


from typing import Optional
from pydantic import BaseModel, Field, StrictInt
from pyk8s.models.v1_exec_action import V1ExecAction
from pyk8s.models.v1_grpc_action import V1GRPCAction
from pyk8s.models.v1_http_get_action import V1HTTPGetAction
from pyk8s.models.v1_tcp_socket_action import V1TCPSocketAction
from pydantic import ValidationError

class V1Probe(BaseModel):
    """NOTE: This class is auto generated by OpenAPI Generator.
    Ref: https://openapi-generator.tech

    Do not edit the class manually.
    """
    var_exec: Optional[V1ExecAction] = Field(None, alias="exec")
    failure_threshold: Optional[StrictInt] = Field(None, alias="failureThreshold", description="Minimum consecutive failures for the probe to be considered failed after having succeeded. Defaults to 3. Minimum value is 1.")
    grpc: Optional[V1GRPCAction] = None
    http_get: Optional[V1HTTPGetAction] = Field(None, alias="httpGet")
    initial_delay_seconds: Optional[StrictInt] = Field(None, alias="initialDelaySeconds", description="Number of seconds after the container has started before liveness probes are initiated. More info: https://kubernetes.io/docs/concepts/workloads/pods/pod-lifecycle#container-probes")
    period_seconds: Optional[StrictInt] = Field(None, alias="periodSeconds", description="How often (in seconds) to perform the probe. Default to 10 seconds. Minimum value is 1.")
    success_threshold: Optional[StrictInt] = Field(None, alias="successThreshold", description="Minimum consecutive successes for the probe to be considered successful after having failed. Defaults to 1. Must be 1 for liveness and startup. Minimum value is 1.")
    tcp_socket: Optional[V1TCPSocketAction] = Field(None, alias="tcpSocket")
    termination_grace_period_seconds: Optional[StrictInt] = Field(None, alias="terminationGracePeriodSeconds", description="Optional duration in seconds the pod needs to terminate gracefully upon probe failure. The grace period is the duration in seconds after the processes running in the pod are sent a termination signal and the time when the processes are forcibly halted with a kill signal. Set this value longer than the expected cleanup time for your process. If this value is nil, the pod's terminationGracePeriodSeconds will be used. Otherwise, this value overrides the value provided by the pod spec. Value must be non-negative integer. The value zero indicates stop immediately via the kill signal (no opportunity to shut down). This is a beta field and requires enabling ProbeTerminationGracePeriod feature gate. Minimum value is 1. spec.terminationGracePeriodSeconds is used if unset.")
    timeout_seconds: Optional[StrictInt] = Field(None, alias="timeoutSeconds", description="Number of seconds after which the probe times out. Defaults to 1 second. Minimum value is 1. More info: https://kubernetes.io/docs/concepts/workloads/pods/pod-lifecycle#container-probes")

    class Config:
        allow_population_by_field_name = True
        validate_assignment = True

    def to_str(self) -> str:
        """Returns the string representation of the model using alias"""
        return pprint.pformat(self.to_dict())

    def to_json(self) -> str:
        """Returns the JSON representation of the model using alias"""
        return json.dumps(self.to_dict())

    @classmethod
    def from_json(cls, json_str: str) -> V1Probe:
        """Create an instance of V1Probe from a JSON string"""
        return cls.from_dict(json.loads(json_str))

    def to_dict(self):
        """Returns the dictionary representation of the model using alias"""
        _dict = self.dict(by_alias=True, exclude_none=True)
        # override the default output from pydantic by calling `to_dict()` of var_exec
        if self.var_exec:
            _dict['exec'] = self.var_exec.to_dict()
        # override the default output from pydantic by calling `to_dict()` of grpc
        if self.grpc:
            _dict['grpc'] = self.grpc.to_dict()
        # override the default output from pydantic by calling `to_dict()` of http_get
        if self.http_get:
            _dict['httpGet'] = self.http_get.to_dict()
        # override the default output from pydantic by calling `to_dict()` of tcp_socket
        if self.tcp_socket:
            _dict['tcpSocket'] = self.tcp_socket.to_dict()

        return _dict

    @classmethod
    def from_dict(cls, obj: dict) -> V1Probe:
        """Create an instance of V1Probe from a dict"""
        if type(obj) is not dict:
            return V1Probe.parse_obj(obj)

        return V1Probe.parse_obj({
            "var_exec": V1ExecAction.from_dict(obj.get("exec")),
            "failure_threshold": obj.get("failureThreshold"),
            "grpc": V1GRPCAction.from_dict(obj.get("grpc")),
            "http_get": V1HTTPGetAction.from_dict(obj.get("httpGet")),
            "initial_delay_seconds": obj.get("initialDelaySeconds"),
            "period_seconds": obj.get("periodSeconds"),
            "success_threshold": obj.get("successThreshold"),
            "tcp_socket": V1TCPSocketAction.from_dict(obj.get("tcpSocket")),
            "termination_grace_period_seconds": obj.get("terminationGracePeriodSeconds"),
            "timeout_seconds": obj.get("timeoutSeconds")
        })



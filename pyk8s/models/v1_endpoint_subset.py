# coding: utf-8

"""
    Kubernetes

    No description provided (generated by Openapi Generator https://github.com/openapitools/openapi-generator)  # noqa: E501

    The version of the OpenAPI document: release-1.25
    Generated by: https://openapi-generator.tech
"""


from __future__ import annotations
from inspect import getfullargspec
import pprint
import re  # noqa: F401
import json


from typing import List, Optional
from pydantic import BaseModel, Field
from pyk8s.models.core_v1_endpoint_port import CoreV1EndpointPort
from pyk8s.models.v1_endpoint_address import V1EndpointAddress
from pydantic import ValidationError

class V1EndpointSubset(BaseModel):
    """NOTE: This class is auto generated by OpenAPI Generator.
    Ref: https://openapi-generator.tech

    Do not edit the class manually.
    """
    addresses: Optional[List[V1EndpointAddress]] = Field(None, description="IP addresses which offer the related ports that are marked as ready. These endpoints should be considered safe for load balancers and clients to utilize.")
    not_ready_addresses: Optional[List[V1EndpointAddress]] = Field(None, alias="notReadyAddresses", description="IP addresses which offer the related ports but are not currently marked as ready because they have not yet finished starting, have recently failed a readiness check, or have recently failed a liveness check.")
    ports: Optional[List[CoreV1EndpointPort]] = Field(None, description="Port numbers available on the related IP addresses.")

    class Config:
        allow_population_by_field_name = True
        validate_assignment = True

    def to_str(self) -> str:
        """Returns the string representation of the model using alias"""
        return pprint.pformat(self.to_dict())

    def to_json(self) -> str:
        """Returns the JSON representation of the model using alias"""
        return json.dumps(self.to_dict())

    @classmethod
    def from_json(cls, json_str: str) -> V1EndpointSubset:
        """Create an instance of V1EndpointSubset from a JSON string"""
        return cls.from_dict(json.loads(json_str))

    def to_dict(self):
        """Returns the dictionary representation of the model using alias"""
        _dict = self.dict(by_alias=True, exclude_none=True)
        # override the default output from pydantic by calling `to_dict()` of each item in addresses (list)
        _items = []
        if self.addresses:
            for _item in self.addresses:
                if _item:
                    _items.append(_item.to_dict())
            _dict['addresses'] = _items
        # override the default output from pydantic by calling `to_dict()` of each item in not_ready_addresses (list)
        _items = []
        if self.not_ready_addresses:
            for _item in self.not_ready_addresses:
                if _item:
                    _items.append(_item.to_dict())
            _dict['notReadyAddresses'] = _items
        # override the default output from pydantic by calling `to_dict()` of each item in ports (list)
        _items = []
        if self.ports:
            for _item in self.ports:
                if _item:
                    _items.append(_item.to_dict())
            _dict['ports'] = _items

        return _dict

    @classmethod
    def from_dict(cls, obj: dict) -> V1EndpointSubset:
        """Create an instance of V1EndpointSubset from a dict"""
        if type(obj) is not dict:
            return V1EndpointSubset.parse_obj(obj)

        return V1EndpointSubset.parse_obj({
            "addresses": [V1EndpointAddress.from_dict(_item) for _item in obj.get("addresses")],
            "not_ready_addresses": [V1EndpointAddress.from_dict(_item) for _item in obj.get("notReadyAddresses")],
            "ports": [CoreV1EndpointPort.from_dict(_item) for _item in obj.get("ports")]
        })



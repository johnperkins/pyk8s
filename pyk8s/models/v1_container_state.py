# coding: utf-8

"""
    Kubernetes

    No description provided (generated by Openapi Generator https://github.com/openapitools/openapi-generator)  # noqa: E501

    The version of the OpenAPI document: release-1.25
    Generated by: https://openapi-generator.tech
"""


from __future__ import annotations
from inspect import getfullargspec
import pprint
import re  # noqa: F401
import json


from typing import Optional
from pydantic import BaseModel
from pyk8s.models.v1_container_state_running import V1ContainerStateRunning
from pyk8s.models.v1_container_state_terminated import V1ContainerStateTerminated
from pyk8s.models.v1_container_state_waiting import V1ContainerStateWaiting
from pydantic import ValidationError

class V1ContainerState(BaseModel):
    """NOTE: This class is auto generated by OpenAPI Generator.
    Ref: https://openapi-generator.tech

    Do not edit the class manually.
    """
    running: Optional[V1ContainerStateRunning] = None
    terminated: Optional[V1ContainerStateTerminated] = None
    waiting: Optional[V1ContainerStateWaiting] = None

    class Config:
        allow_population_by_field_name = True
        validate_assignment = True

    def to_str(self) -> str:
        """Returns the string representation of the model using alias"""
        return pprint.pformat(self.to_dict())

    def to_json(self) -> str:
        """Returns the JSON representation of the model using alias"""
        return json.dumps(self.to_dict())

    @classmethod
    def from_json(cls, json_str: str) -> V1ContainerState:
        """Create an instance of V1ContainerState from a JSON string"""
        return cls.from_dict(json.loads(json_str))

    def to_dict(self):
        """Returns the dictionary representation of the model using alias"""
        _dict = self.dict(by_alias=True, exclude_none=True)
        # override the default output from pydantic by calling `to_dict()` of running
        if self.running:
            _dict['running'] = self.running.to_dict()
        # override the default output from pydantic by calling `to_dict()` of terminated
        if self.terminated:
            _dict['terminated'] = self.terminated.to_dict()
        # override the default output from pydantic by calling `to_dict()` of waiting
        if self.waiting:
            _dict['waiting'] = self.waiting.to_dict()

        return _dict

    @classmethod
    def from_dict(cls, obj: dict) -> V1ContainerState:
        """Create an instance of V1ContainerState from a dict"""
        if type(obj) is not dict:
            return V1ContainerState.parse_obj(obj)

        return V1ContainerState.parse_obj({
            "running": V1ContainerStateRunning.from_dict(obj.get("running")),
            "terminated": V1ContainerStateTerminated.from_dict(obj.get("terminated")),
            "waiting": V1ContainerStateWaiting.from_dict(obj.get("waiting"))
        })



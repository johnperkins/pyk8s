# coding: utf-8

"""
    Kubernetes

    No description provided (generated by Openapi Generator https://github.com/openapitools/openapi-generator)  # noqa: E501

    The version of the OpenAPI document: release-1.25
    Generated by: https://openapi-generator.tech
"""


from __future__ import annotations
from inspect import getfullargspec
import pprint
import re  # noqa: F401
import json


from typing import Dict, Optional
from pydantic import BaseModel, Field, StrictStr
from pydantic import ValidationError

class V1LimitRangeItem(BaseModel):
    """NOTE: This class is auto generated by OpenAPI Generator.
    Ref: https://openapi-generator.tech

    Do not edit the class manually.
    """
    default: Optional[Dict[str, StrictStr]] = Field(None, description="Default resource requirement limit value by resource name if resource limit is omitted.")
    default_request: Optional[Dict[str, StrictStr]] = Field(None, alias="defaultRequest", description="DefaultRequest is the default resource requirement request value by resource name if resource request is omitted.")
    max: Optional[Dict[str, StrictStr]] = Field(None, description="Max usage constraints on this kind by resource name.")
    max_limit_request_ratio: Optional[Dict[str, StrictStr]] = Field(None, alias="maxLimitRequestRatio", description="MaxLimitRequestRatio if specified, the named resource must have a request and limit that are both non-zero where limit divided by request is less than or equal to the enumerated value; this represents the max burst for the named resource.")
    min: Optional[Dict[str, StrictStr]] = Field(None, description="Min usage constraints on this kind by resource name.")
    type: StrictStr = Field(..., description="Type of resource that this limit applies to.")

    class Config:
        allow_population_by_field_name = True
        validate_assignment = True

    def to_str(self) -> str:
        """Returns the string representation of the model using alias"""
        return pprint.pformat(self.to_dict())

    def to_json(self) -> str:
        """Returns the JSON representation of the model using alias"""
        return json.dumps(self.to_dict())

    @classmethod
    def from_json(cls, json_str: str) -> V1LimitRangeItem:
        """Create an instance of V1LimitRangeItem from a JSON string"""
        return cls.from_dict(json.loads(json_str))

    def to_dict(self):
        """Returns the dictionary representation of the model using alias"""
        _dict = self.dict(by_alias=True, exclude_none=True)

        return _dict

    @classmethod
    def from_dict(cls, obj: dict) -> V1LimitRangeItem:
        """Create an instance of V1LimitRangeItem from a dict"""
        if type(obj) is not dict:
            return V1LimitRangeItem.parse_obj(obj)

        return V1LimitRangeItem.parse_obj({
            "default": obj.get("default"),
            "default_request": obj.get("defaultRequest"),
            "max": obj.get("max"),
            "max_limit_request_ratio": obj.get("maxLimitRequestRatio"),
            "min": obj.get("min"),
            "type": obj.get("type")
        })



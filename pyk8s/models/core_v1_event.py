# coding: utf-8

"""
    Kubernetes

    No description provided (generated by Openapi Generator https://github.com/openapitools/openapi-generator)  # noqa: E501

    The version of the OpenAPI document: release-1.25
    Generated by: https://openapi-generator.tech
"""


from __future__ import annotations
from inspect import getfullargspec
import pprint
import re  # noqa: F401
import json

from datetime import datetime
from typing import Optional
from pydantic import BaseModel, Field, StrictInt, StrictStr
from pyk8s.models.core_v1_event_series import CoreV1EventSeries
from pyk8s.models.v1_event_source import V1EventSource
from pyk8s.models.v1_object_meta import V1ObjectMeta
from pyk8s.models.v1_object_reference import V1ObjectReference
from pydantic import ValidationError

class CoreV1Event(BaseModel):
    """NOTE: This class is auto generated by OpenAPI Generator.
    Ref: https://openapi-generator.tech

    Do not edit the class manually.
    """
    action: Optional[StrictStr] = Field(None, description="What action was taken/failed regarding to the Regarding object.")
    api_version: Optional[StrictStr] = Field(None, alias="apiVersion", description="APIVersion defines the versioned schema of this representation of an object. Servers should convert recognized schemas to the latest internal value, and may reject unrecognized values. More info: https://git.k8s.io/community/contributors/devel/sig-architecture/api-conventions.md#resources")
    count: Optional[StrictInt] = Field(None, description="The number of times this event has occurred.")
    event_time: Optional[datetime] = Field(None, alias="eventTime", description="Time when this Event was first observed.")
    first_timestamp: Optional[datetime] = Field(None, alias="firstTimestamp", description="The time at which the event was first recorded. (Time of server receipt is in TypeMeta.)")
    involved_object: V1ObjectReference = Field(..., alias="involvedObject")
    kind: Optional[StrictStr] = Field(None, description="Kind is a string value representing the REST resource this object represents. Servers may infer this from the endpoint the client submits requests to. Cannot be updated. In CamelCase. More info: https://git.k8s.io/community/contributors/devel/sig-architecture/api-conventions.md#types-kinds")
    last_timestamp: Optional[datetime] = Field(None, alias="lastTimestamp", description="The time at which the most recent occurrence of this event was recorded.")
    message: Optional[StrictStr] = Field(None, description="A human-readable description of the status of this operation.")
    metadata: V1ObjectMeta = ...
    reason: Optional[StrictStr] = Field(None, description="This should be a short, machine understandable string that gives the reason for the transition into the object's current status.")
    related: Optional[V1ObjectReference] = None
    reporting_component: Optional[StrictStr] = Field(None, alias="reportingComponent", description="Name of the controller that emitted this Event, e.g. `kubernetes.io/kubelet`.")
    reporting_instance: Optional[StrictStr] = Field(None, alias="reportingInstance", description="ID of the controller instance, e.g. `kubelet-xyzf`.")
    series: Optional[CoreV1EventSeries] = None
    source: Optional[V1EventSource] = None
    type: Optional[StrictStr] = Field(None, description="Type of this event (Normal, Warning), new types could be added in the future")

    class Config:
        allow_population_by_field_name = True
        validate_assignment = True

    def to_str(self) -> str:
        """Returns the string representation of the model using alias"""
        return pprint.pformat(self.to_dict())

    def to_json(self) -> str:
        """Returns the JSON representation of the model using alias"""
        return json.dumps(self.to_dict())

    @classmethod
    def from_json(cls, json_str: str) -> CoreV1Event:
        """Create an instance of CoreV1Event from a JSON string"""
        return cls.from_dict(json.loads(json_str))

    def to_dict(self):
        """Returns the dictionary representation of the model using alias"""
        _dict = self.dict(by_alias=True, exclude_none=True)
        # override the default output from pydantic by calling `to_dict()` of involved_object
        if self.involved_object:
            _dict['involvedObject'] = self.involved_object.to_dict()
        # override the default output from pydantic by calling `to_dict()` of metadata
        if self.metadata:
            _dict['metadata'] = self.metadata.to_dict()
        # override the default output from pydantic by calling `to_dict()` of related
        if self.related:
            _dict['related'] = self.related.to_dict()
        # override the default output from pydantic by calling `to_dict()` of series
        if self.series:
            _dict['series'] = self.series.to_dict()
        # override the default output from pydantic by calling `to_dict()` of source
        if self.source:
            _dict['source'] = self.source.to_dict()

        return _dict

    @classmethod
    def from_dict(cls, obj: dict) -> CoreV1Event:
        """Create an instance of CoreV1Event from a dict"""
        if type(obj) is not dict:
            return CoreV1Event.parse_obj(obj)

        return CoreV1Event.parse_obj({
            "action": obj.get("action"),
            "api_version": obj.get("apiVersion"),
            "count": obj.get("count"),
            "event_time": obj.get("eventTime"),
            "first_timestamp": obj.get("firstTimestamp"),
            "involved_object": V1ObjectReference.from_dict(obj.get("involvedObject")),
            "kind": obj.get("kind"),
            "last_timestamp": obj.get("lastTimestamp"),
            "message": obj.get("message"),
            "metadata": V1ObjectMeta.from_dict(obj.get("metadata")),
            "reason": obj.get("reason"),
            "related": V1ObjectReference.from_dict(obj.get("related")),
            "reporting_component": obj.get("reportingComponent"),
            "reporting_instance": obj.get("reportingInstance"),
            "series": CoreV1EventSeries.from_dict(obj.get("series")),
            "source": V1EventSource.from_dict(obj.get("source")),
            "type": obj.get("type")
        })



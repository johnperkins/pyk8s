# coding: utf-8

"""
    Kubernetes

    No description provided (generated by Openapi Generator https://github.com/openapitools/openapi-generator)  # noqa: E501

    The version of the OpenAPI document: release-1.25
    Generated by: https://openapi-generator.tech
"""


from __future__ import annotations
from inspect import getfullargspec
import pprint
import re  # noqa: F401
import json


from typing import Dict, List, Optional
from pydantic import BaseModel, Field, StrictBool, StrictStr
from pyk8s.models.v1_object_meta import V1ObjectMeta
from pyk8s.models.v1_topology_selector_term import V1TopologySelectorTerm
from pydantic import ValidationError

class V1StorageClass(BaseModel):
    """NOTE: This class is auto generated by OpenAPI Generator.
    Ref: https://openapi-generator.tech

    Do not edit the class manually.
    """
    allow_volume_expansion: Optional[StrictBool] = Field(None, alias="allowVolumeExpansion", description="AllowVolumeExpansion shows whether the storage class allow volume expand")
    allowed_topologies: Optional[List[V1TopologySelectorTerm]] = Field(None, alias="allowedTopologies", description="Restrict the node topologies where volumes can be dynamically provisioned. Each volume plugin defines its own supported topology specifications. An empty TopologySelectorTerm list means there is no topology restriction. This field is only honored by servers that enable the VolumeScheduling feature.")
    api_version: Optional[StrictStr] = Field(None, alias="apiVersion", description="APIVersion defines the versioned schema of this representation of an object. Servers should convert recognized schemas to the latest internal value, and may reject unrecognized values. More info: https://git.k8s.io/community/contributors/devel/sig-architecture/api-conventions.md#resources")
    kind: Optional[StrictStr] = Field(None, description="Kind is a string value representing the REST resource this object represents. Servers may infer this from the endpoint the client submits requests to. Cannot be updated. In CamelCase. More info: https://git.k8s.io/community/contributors/devel/sig-architecture/api-conventions.md#types-kinds")
    metadata: Optional[V1ObjectMeta] = None
    mount_options: Optional[List[StrictStr]] = Field(None, alias="mountOptions", description="Dynamically provisioned PersistentVolumes of this storage class are created with these mountOptions, e.g. [\"ro\", \"soft\"]. Not validated - mount of the PVs will simply fail if one is invalid.")
    parameters: Optional[Dict[str, StrictStr]] = Field(None, description="Parameters holds the parameters for the provisioner that should create volumes of this storage class.")
    provisioner: StrictStr = Field(..., description="Provisioner indicates the type of the provisioner.")
    reclaim_policy: Optional[StrictStr] = Field(None, alias="reclaimPolicy", description="Dynamically provisioned PersistentVolumes of this storage class are created with this reclaimPolicy. Defaults to Delete.")
    volume_binding_mode: Optional[StrictStr] = Field(None, alias="volumeBindingMode", description="VolumeBindingMode indicates how PersistentVolumeClaims should be provisioned and bound.  When unset, VolumeBindingImmediate is used. This field is only honored by servers that enable the VolumeScheduling feature.")

    class Config:
        allow_population_by_field_name = True
        validate_assignment = True

    def to_str(self) -> str:
        """Returns the string representation of the model using alias"""
        return pprint.pformat(self.to_dict())

    def to_json(self) -> str:
        """Returns the JSON representation of the model using alias"""
        return json.dumps(self.to_dict())

    @classmethod
    def from_json(cls, json_str: str) -> V1StorageClass:
        """Create an instance of V1StorageClass from a JSON string"""
        return cls.from_dict(json.loads(json_str))

    def to_dict(self):
        """Returns the dictionary representation of the model using alias"""
        _dict = self.dict(by_alias=True, exclude_none=True)
        # override the default output from pydantic by calling `to_dict()` of each item in allowed_topologies (list)
        _items = []
        if self.allowed_topologies:
            for _item in self.allowed_topologies:
                if _item:
                    _items.append(_item.to_dict())
            _dict['allowedTopologies'] = _items
        # override the default output from pydantic by calling `to_dict()` of metadata
        if self.metadata:
            _dict['metadata'] = self.metadata.to_dict()

        return _dict

    @classmethod
    def from_dict(cls, obj: dict) -> V1StorageClass:
        """Create an instance of V1StorageClass from a dict"""
        if type(obj) is not dict:
            return V1StorageClass.parse_obj(obj)

        return V1StorageClass.parse_obj({
            "allow_volume_expansion": obj.get("allowVolumeExpansion"),
            "allowed_topologies": [V1TopologySelectorTerm.from_dict(_item) for _item in obj.get("allowedTopologies")],
            "api_version": obj.get("apiVersion"),
            "kind": obj.get("kind"),
            "metadata": V1ObjectMeta.from_dict(obj.get("metadata")),
            "mount_options": obj.get("mountOptions"),
            "parameters": obj.get("parameters"),
            "provisioner": obj.get("provisioner"),
            "reclaim_policy": obj.get("reclaimPolicy"),
            "volume_binding_mode": obj.get("volumeBindingMode")
        })



# coding: utf-8

"""
    Kubernetes

    No description provided (generated by Openapi Generator https://github.com/openapitools/openapi-generator)  # noqa: E501

    The version of the OpenAPI document: release-1.25
    Generated by: https://openapi-generator.tech
"""


from __future__ import annotations
from inspect import getfullargspec
import pprint
import re  # noqa: F401
import json


from typing import Dict, List, Optional
from pydantic import BaseModel, Field, StrictStr
from pyk8s.models.v1_non_resource_attributes import V1NonResourceAttributes
from pyk8s.models.v1_resource_attributes import V1ResourceAttributes
from pydantic import ValidationError

class V1SubjectAccessReviewSpec(BaseModel):
    """NOTE: This class is auto generated by OpenAPI Generator.
    Ref: https://openapi-generator.tech

    Do not edit the class manually.
    """
    extra: Optional[Dict[str, List[StrictStr]]] = Field(None, description="Extra corresponds to the user.Info.GetExtra() method from the authenticator.  Since that is input to the authorizer it needs a reflection here.")
    groups: Optional[List[StrictStr]] = Field(None, description="Groups is the groups you're testing for.")
    non_resource_attributes: Optional[V1NonResourceAttributes] = Field(None, alias="nonResourceAttributes")
    resource_attributes: Optional[V1ResourceAttributes] = Field(None, alias="resourceAttributes")
    uid: Optional[StrictStr] = Field(None, description="UID information about the requesting user.")
    user: Optional[StrictStr] = Field(None, description="User is the user you're testing for. If you specify \"User\" but not \"Groups\", then is it interpreted as \"What if User were not a member of any groups")

    class Config:
        allow_population_by_field_name = True
        validate_assignment = True

    def to_str(self) -> str:
        """Returns the string representation of the model using alias"""
        return pprint.pformat(self.to_dict())

    def to_json(self) -> str:
        """Returns the JSON representation of the model using alias"""
        return json.dumps(self.to_dict())

    @classmethod
    def from_json(cls, json_str: str) -> V1SubjectAccessReviewSpec:
        """Create an instance of V1SubjectAccessReviewSpec from a JSON string"""
        return cls.from_dict(json.loads(json_str))

    def to_dict(self):
        """Returns the dictionary representation of the model using alias"""
        _dict = self.dict(by_alias=True, exclude_none=True)
        # override the default output from pydantic by calling `to_dict()` of non_resource_attributes
        if self.non_resource_attributes:
            _dict['nonResourceAttributes'] = self.non_resource_attributes.to_dict()
        # override the default output from pydantic by calling `to_dict()` of resource_attributes
        if self.resource_attributes:
            _dict['resourceAttributes'] = self.resource_attributes.to_dict()

        return _dict

    @classmethod
    def from_dict(cls, obj: dict) -> V1SubjectAccessReviewSpec:
        """Create an instance of V1SubjectAccessReviewSpec from a dict"""
        if type(obj) is not dict:
            return V1SubjectAccessReviewSpec.parse_obj(obj)

        return V1SubjectAccessReviewSpec.parse_obj({
            "extra": obj.get("extra"),
            "groups": obj.get("groups"),
            "non_resource_attributes": V1NonResourceAttributes.from_dict(obj.get("nonResourceAttributes")),
            "resource_attributes": V1ResourceAttributes.from_dict(obj.get("resourceAttributes")),
            "uid": obj.get("uid"),
            "user": obj.get("user")
        })



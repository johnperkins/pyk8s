# coding: utf-8

"""
    Kubernetes

    No description provided (generated by Openapi Generator https://github.com/openapitools/openapi-generator)  # noqa: E501

    The version of the OpenAPI document: release-1.25
    Generated by: https://openapi-generator.tech
"""


from __future__ import annotations
from inspect import getfullargspec
import pprint
import re  # noqa: F401
import json


from typing import Dict, List, Optional
from pydantic import BaseModel, Field, StrictStr
from pyk8s.models.v1_aws_elastic_block_store_volume_source import V1AWSElasticBlockStoreVolumeSource
from pyk8s.models.v1_azure_disk_volume_source import V1AzureDiskVolumeSource
from pyk8s.models.v1_azure_file_persistent_volume_source import V1AzureFilePersistentVolumeSource
from pyk8s.models.v1_ceph_fs_persistent_volume_source import V1CephFSPersistentVolumeSource
from pyk8s.models.v1_cinder_persistent_volume_source import V1CinderPersistentVolumeSource
from pyk8s.models.v1_csi_persistent_volume_source import V1CSIPersistentVolumeSource
from pyk8s.models.v1_fc_volume_source import V1FCVolumeSource
from pyk8s.models.v1_flex_persistent_volume_source import V1FlexPersistentVolumeSource
from pyk8s.models.v1_flocker_volume_source import V1FlockerVolumeSource
from pyk8s.models.v1_gce_persistent_disk_volume_source import V1GCEPersistentDiskVolumeSource
from pyk8s.models.v1_glusterfs_persistent_volume_source import V1GlusterfsPersistentVolumeSource
from pyk8s.models.v1_host_path_volume_source import V1HostPathVolumeSource
from pyk8s.models.v1_iscsi_persistent_volume_source import V1ISCSIPersistentVolumeSource
from pyk8s.models.v1_local_volume_source import V1LocalVolumeSource
from pyk8s.models.v1_nfs_volume_source import V1NFSVolumeSource
from pyk8s.models.v1_object_reference import V1ObjectReference
from pyk8s.models.v1_photon_persistent_disk_volume_source import V1PhotonPersistentDiskVolumeSource
from pyk8s.models.v1_portworx_volume_source import V1PortworxVolumeSource
from pyk8s.models.v1_quobyte_volume_source import V1QuobyteVolumeSource
from pyk8s.models.v1_rbd_persistent_volume_source import V1RBDPersistentVolumeSource
from pyk8s.models.v1_scale_io_persistent_volume_source import V1ScaleIOPersistentVolumeSource
from pyk8s.models.v1_storage_os_persistent_volume_source import V1StorageOSPersistentVolumeSource
from pyk8s.models.v1_volume_node_affinity import V1VolumeNodeAffinity
from pyk8s.models.v1_vsphere_virtual_disk_volume_source import V1VsphereVirtualDiskVolumeSource
from pydantic import ValidationError

class V1PersistentVolumeSpec(BaseModel):
    """NOTE: This class is auto generated by OpenAPI Generator.
    Ref: https://openapi-generator.tech

    Do not edit the class manually.
    """
    access_modes: Optional[List[StrictStr]] = Field(None, alias="accessModes", description="accessModes contains all ways the volume can be mounted. More info: https://kubernetes.io/docs/concepts/storage/persistent-volumes#access-modes")
    aws_elastic_block_store: Optional[V1AWSElasticBlockStoreVolumeSource] = Field(None, alias="awsElasticBlockStore")
    azure_disk: Optional[V1AzureDiskVolumeSource] = Field(None, alias="azureDisk")
    azure_file: Optional[V1AzureFilePersistentVolumeSource] = Field(None, alias="azureFile")
    capacity: Optional[Dict[str, StrictStr]] = Field(None, description="capacity is the description of the persistent volume's resources and capacity. More info: https://kubernetes.io/docs/concepts/storage/persistent-volumes#capacity")
    cephfs: Optional[V1CephFSPersistentVolumeSource] = None
    cinder: Optional[V1CinderPersistentVolumeSource] = None
    claim_ref: Optional[V1ObjectReference] = Field(None, alias="claimRef")
    csi: Optional[V1CSIPersistentVolumeSource] = None
    fc: Optional[V1FCVolumeSource] = None
    flex_volume: Optional[V1FlexPersistentVolumeSource] = Field(None, alias="flexVolume")
    flocker: Optional[V1FlockerVolumeSource] = None
    gce_persistent_disk: Optional[V1GCEPersistentDiskVolumeSource] = Field(None, alias="gcePersistentDisk")
    glusterfs: Optional[V1GlusterfsPersistentVolumeSource] = None
    host_path: Optional[V1HostPathVolumeSource] = Field(None, alias="hostPath")
    iscsi: Optional[V1ISCSIPersistentVolumeSource] = None
    local: Optional[V1LocalVolumeSource] = None
    mount_options: Optional[List[StrictStr]] = Field(None, alias="mountOptions", description="mountOptions is the list of mount options, e.g. [\"ro\", \"soft\"]. Not validated - mount will simply fail if one is invalid. More info: https://kubernetes.io/docs/concepts/storage/persistent-volumes/#mount-options")
    nfs: Optional[V1NFSVolumeSource] = None
    node_affinity: Optional[V1VolumeNodeAffinity] = Field(None, alias="nodeAffinity")
    persistent_volume_reclaim_policy: Optional[StrictStr] = Field(None, alias="persistentVolumeReclaimPolicy", description="persistentVolumeReclaimPolicy defines what happens to a persistent volume when released from its claim. Valid options are Retain (default for manually created PersistentVolumes), Delete (default for dynamically provisioned PersistentVolumes), and Recycle (deprecated). Recycle must be supported by the volume plugin underlying this PersistentVolume. More info: https://kubernetes.io/docs/concepts/storage/persistent-volumes#reclaiming  ")
    photon_persistent_disk: Optional[V1PhotonPersistentDiskVolumeSource] = Field(None, alias="photonPersistentDisk")
    portworx_volume: Optional[V1PortworxVolumeSource] = Field(None, alias="portworxVolume")
    quobyte: Optional[V1QuobyteVolumeSource] = None
    rbd: Optional[V1RBDPersistentVolumeSource] = None
    scale_io: Optional[V1ScaleIOPersistentVolumeSource] = Field(None, alias="scaleIO")
    storage_class_name: Optional[StrictStr] = Field(None, alias="storageClassName", description="storageClassName is the name of StorageClass to which this persistent volume belongs. Empty value means that this volume does not belong to any StorageClass.")
    storageos: Optional[V1StorageOSPersistentVolumeSource] = None
    volume_mode: Optional[StrictStr] = Field(None, alias="volumeMode", description="volumeMode defines if a volume is intended to be used with a formatted filesystem or to remain in raw block state. Value of Filesystem is implied when not included in spec.")
    vsphere_volume: Optional[V1VsphereVirtualDiskVolumeSource] = Field(None, alias="vsphereVolume")

    class Config:
        allow_population_by_field_name = True
        validate_assignment = True

    def to_str(self) -> str:
        """Returns the string representation of the model using alias"""
        return pprint.pformat(self.to_dict())

    def to_json(self) -> str:
        """Returns the JSON representation of the model using alias"""
        return json.dumps(self.to_dict())

    @classmethod
    def from_json(cls, json_str: str) -> V1PersistentVolumeSpec:
        """Create an instance of V1PersistentVolumeSpec from a JSON string"""
        return cls.from_dict(json.loads(json_str))

    def to_dict(self):
        """Returns the dictionary representation of the model using alias"""
        _dict = self.dict(by_alias=True, exclude_none=True)
        # override the default output from pydantic by calling `to_dict()` of aws_elastic_block_store
        if self.aws_elastic_block_store:
            _dict['awsElasticBlockStore'] = self.aws_elastic_block_store.to_dict()
        # override the default output from pydantic by calling `to_dict()` of azure_disk
        if self.azure_disk:
            _dict['azureDisk'] = self.azure_disk.to_dict()
        # override the default output from pydantic by calling `to_dict()` of azure_file
        if self.azure_file:
            _dict['azureFile'] = self.azure_file.to_dict()
        # override the default output from pydantic by calling `to_dict()` of cephfs
        if self.cephfs:
            _dict['cephfs'] = self.cephfs.to_dict()
        # override the default output from pydantic by calling `to_dict()` of cinder
        if self.cinder:
            _dict['cinder'] = self.cinder.to_dict()
        # override the default output from pydantic by calling `to_dict()` of claim_ref
        if self.claim_ref:
            _dict['claimRef'] = self.claim_ref.to_dict()
        # override the default output from pydantic by calling `to_dict()` of csi
        if self.csi:
            _dict['csi'] = self.csi.to_dict()
        # override the default output from pydantic by calling `to_dict()` of fc
        if self.fc:
            _dict['fc'] = self.fc.to_dict()
        # override the default output from pydantic by calling `to_dict()` of flex_volume
        if self.flex_volume:
            _dict['flexVolume'] = self.flex_volume.to_dict()
        # override the default output from pydantic by calling `to_dict()` of flocker
        if self.flocker:
            _dict['flocker'] = self.flocker.to_dict()
        # override the default output from pydantic by calling `to_dict()` of gce_persistent_disk
        if self.gce_persistent_disk:
            _dict['gcePersistentDisk'] = self.gce_persistent_disk.to_dict()
        # override the default output from pydantic by calling `to_dict()` of glusterfs
        if self.glusterfs:
            _dict['glusterfs'] = self.glusterfs.to_dict()
        # override the default output from pydantic by calling `to_dict()` of host_path
        if self.host_path:
            _dict['hostPath'] = self.host_path.to_dict()
        # override the default output from pydantic by calling `to_dict()` of iscsi
        if self.iscsi:
            _dict['iscsi'] = self.iscsi.to_dict()
        # override the default output from pydantic by calling `to_dict()` of local
        if self.local:
            _dict['local'] = self.local.to_dict()
        # override the default output from pydantic by calling `to_dict()` of nfs
        if self.nfs:
            _dict['nfs'] = self.nfs.to_dict()
        # override the default output from pydantic by calling `to_dict()` of node_affinity
        if self.node_affinity:
            _dict['nodeAffinity'] = self.node_affinity.to_dict()
        # override the default output from pydantic by calling `to_dict()` of photon_persistent_disk
        if self.photon_persistent_disk:
            _dict['photonPersistentDisk'] = self.photon_persistent_disk.to_dict()
        # override the default output from pydantic by calling `to_dict()` of portworx_volume
        if self.portworx_volume:
            _dict['portworxVolume'] = self.portworx_volume.to_dict()
        # override the default output from pydantic by calling `to_dict()` of quobyte
        if self.quobyte:
            _dict['quobyte'] = self.quobyte.to_dict()
        # override the default output from pydantic by calling `to_dict()` of rbd
        if self.rbd:
            _dict['rbd'] = self.rbd.to_dict()
        # override the default output from pydantic by calling `to_dict()` of scale_io
        if self.scale_io:
            _dict['scaleIO'] = self.scale_io.to_dict()
        # override the default output from pydantic by calling `to_dict()` of storageos
        if self.storageos:
            _dict['storageos'] = self.storageos.to_dict()
        # override the default output from pydantic by calling `to_dict()` of vsphere_volume
        if self.vsphere_volume:
            _dict['vsphereVolume'] = self.vsphere_volume.to_dict()

        return _dict

    @classmethod
    def from_dict(cls, obj: dict) -> V1PersistentVolumeSpec:
        """Create an instance of V1PersistentVolumeSpec from a dict"""
        if type(obj) is not dict:
            return V1PersistentVolumeSpec.parse_obj(obj)

        return V1PersistentVolumeSpec.parse_obj({
            "access_modes": obj.get("accessModes"),
            "aws_elastic_block_store": V1AWSElasticBlockStoreVolumeSource.from_dict(obj.get("awsElasticBlockStore")),
            "azure_disk": V1AzureDiskVolumeSource.from_dict(obj.get("azureDisk")),
            "azure_file": V1AzureFilePersistentVolumeSource.from_dict(obj.get("azureFile")),
            "capacity": obj.get("capacity"),
            "cephfs": V1CephFSPersistentVolumeSource.from_dict(obj.get("cephfs")),
            "cinder": V1CinderPersistentVolumeSource.from_dict(obj.get("cinder")),
            "claim_ref": V1ObjectReference.from_dict(obj.get("claimRef")),
            "csi": V1CSIPersistentVolumeSource.from_dict(obj.get("csi")),
            "fc": V1FCVolumeSource.from_dict(obj.get("fc")),
            "flex_volume": V1FlexPersistentVolumeSource.from_dict(obj.get("flexVolume")),
            "flocker": V1FlockerVolumeSource.from_dict(obj.get("flocker")),
            "gce_persistent_disk": V1GCEPersistentDiskVolumeSource.from_dict(obj.get("gcePersistentDisk")),
            "glusterfs": V1GlusterfsPersistentVolumeSource.from_dict(obj.get("glusterfs")),
            "host_path": V1HostPathVolumeSource.from_dict(obj.get("hostPath")),
            "iscsi": V1ISCSIPersistentVolumeSource.from_dict(obj.get("iscsi")),
            "local": V1LocalVolumeSource.from_dict(obj.get("local")),
            "mount_options": obj.get("mountOptions"),
            "nfs": V1NFSVolumeSource.from_dict(obj.get("nfs")),
            "node_affinity": V1VolumeNodeAffinity.from_dict(obj.get("nodeAffinity")),
            "persistent_volume_reclaim_policy": obj.get("persistentVolumeReclaimPolicy"),
            "photon_persistent_disk": V1PhotonPersistentDiskVolumeSource.from_dict(obj.get("photonPersistentDisk")),
            "portworx_volume": V1PortworxVolumeSource.from_dict(obj.get("portworxVolume")),
            "quobyte": V1QuobyteVolumeSource.from_dict(obj.get("quobyte")),
            "rbd": V1RBDPersistentVolumeSource.from_dict(obj.get("rbd")),
            "scale_io": V1ScaleIOPersistentVolumeSource.from_dict(obj.get("scaleIO")),
            "storage_class_name": obj.get("storageClassName"),
            "storageos": V1StorageOSPersistentVolumeSource.from_dict(obj.get("storageos")),
            "volume_mode": obj.get("volumeMode"),
            "vsphere_volume": V1VsphereVirtualDiskVolumeSource.from_dict(obj.get("vsphereVolume"))
        })



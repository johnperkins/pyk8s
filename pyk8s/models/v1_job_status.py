# coding: utf-8

"""
    Kubernetes

    No description provided (generated by Openapi Generator https://github.com/openapitools/openapi-generator)  # noqa: E501

    The version of the OpenAPI document: release-1.25
    Generated by: https://openapi-generator.tech
"""


from __future__ import annotations
from inspect import getfullargspec
import pprint
import re  # noqa: F401
import json

from datetime import datetime
from typing import List, Optional
from pydantic import BaseModel, Field, StrictInt, StrictStr
from pyk8s.models.v1_job_condition import V1JobCondition
from pyk8s.models.v1_uncounted_terminated_pods import V1UncountedTerminatedPods
from pydantic import ValidationError

class V1JobStatus(BaseModel):
    """NOTE: This class is auto generated by OpenAPI Generator.
    Ref: https://openapi-generator.tech

    Do not edit the class manually.
    """
    active: Optional[StrictInt] = Field(None, description="The number of pending and running pods.")
    completed_indexes: Optional[StrictStr] = Field(None, alias="completedIndexes", description="CompletedIndexes holds the completed indexes when .spec.completionMode = \"Indexed\" in a text format. The indexes are represented as decimal integers separated by commas. The numbers are listed in increasing order. Three or more consecutive numbers are compressed and represented by the first and last element of the series, separated by a hyphen. For example, if the completed indexes are 1, 3, 4, 5 and 7, they are represented as \"1,3-5,7\".")
    completion_time: Optional[datetime] = Field(None, alias="completionTime", description="Represents time when the job was completed. It is not guaranteed to be set in happens-before order across separate operations. It is represented in RFC3339 form and is in UTC. The completion time is only set when the job finishes successfully.")
    conditions: Optional[List[V1JobCondition]] = Field(None, description="The latest available observations of an object's current state. When a Job fails, one of the conditions will have type \"Failed\" and status true. When a Job is suspended, one of the conditions will have type \"Suspended\" and status true; when the Job is resumed, the status of this condition will become false. When a Job is completed, one of the conditions will have type \"Complete\" and status true. More info: https://kubernetes.io/docs/concepts/workloads/controllers/jobs-run-to-completion/")
    failed: Optional[StrictInt] = Field(None, description="The number of pods which reached phase Failed.")
    ready: Optional[StrictInt] = Field(None, description="The number of pods which have a Ready condition.  This field is beta-level. The job controller populates the field when the feature gate JobReadyPods is enabled (enabled by default).")
    start_time: Optional[datetime] = Field(None, alias="startTime", description="Represents time when the job controller started processing a job. When a Job is created in the suspended state, this field is not set until the first time it is resumed. This field is reset every time a Job is resumed from suspension. It is represented in RFC3339 form and is in UTC.")
    succeeded: Optional[StrictInt] = Field(None, description="The number of pods which reached phase Succeeded.")
    uncounted_terminated_pods: Optional[V1UncountedTerminatedPods] = Field(None, alias="uncountedTerminatedPods")

    class Config:
        allow_population_by_field_name = True
        validate_assignment = True

    def to_str(self) -> str:
        """Returns the string representation of the model using alias"""
        return pprint.pformat(self.to_dict())

    def to_json(self) -> str:
        """Returns the JSON representation of the model using alias"""
        return json.dumps(self.to_dict())

    @classmethod
    def from_json(cls, json_str: str) -> V1JobStatus:
        """Create an instance of V1JobStatus from a JSON string"""
        return cls.from_dict(json.loads(json_str))

    def to_dict(self):
        """Returns the dictionary representation of the model using alias"""
        _dict = self.dict(by_alias=True, exclude_none=True)
        # override the default output from pydantic by calling `to_dict()` of each item in conditions (list)
        _items = []
        if self.conditions:
            for _item in self.conditions:
                if _item:
                    _items.append(_item.to_dict())
            _dict['conditions'] = _items
        # override the default output from pydantic by calling `to_dict()` of uncounted_terminated_pods
        if self.uncounted_terminated_pods:
            _dict['uncountedTerminatedPods'] = self.uncounted_terminated_pods.to_dict()

        return _dict

    @classmethod
    def from_dict(cls, obj: dict) -> V1JobStatus:
        """Create an instance of V1JobStatus from a dict"""
        if type(obj) is not dict:
            return V1JobStatus.parse_obj(obj)

        return V1JobStatus.parse_obj({
            "active": obj.get("active"),
            "completed_indexes": obj.get("completedIndexes"),
            "completion_time": obj.get("completionTime"),
            "conditions": [V1JobCondition.from_dict(_item) for _item in obj.get("conditions")],
            "failed": obj.get("failed"),
            "ready": obj.get("ready"),
            "start_time": obj.get("startTime"),
            "succeeded": obj.get("succeeded"),
            "uncounted_terminated_pods": V1UncountedTerminatedPods.from_dict(obj.get("uncountedTerminatedPods"))
        })



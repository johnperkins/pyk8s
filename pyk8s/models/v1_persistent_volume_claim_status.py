# coding: utf-8

"""
    Kubernetes

    No description provided (generated by Openapi Generator https://github.com/openapitools/openapi-generator)  # noqa: E501

    The version of the OpenAPI document: release-1.25
    Generated by: https://openapi-generator.tech
"""


from __future__ import annotations
from inspect import getfullargspec
import pprint
import re  # noqa: F401
import json


from typing import Dict, List, Optional
from pydantic import BaseModel, Field, StrictStr
from pyk8s.models.v1_persistent_volume_claim_condition import V1PersistentVolumeClaimCondition
from pydantic import ValidationError

class V1PersistentVolumeClaimStatus(BaseModel):
    """NOTE: This class is auto generated by OpenAPI Generator.
    Ref: https://openapi-generator.tech

    Do not edit the class manually.
    """
    access_modes: Optional[List[StrictStr]] = Field(None, alias="accessModes", description="accessModes contains the actual access modes the volume backing the PVC has. More info: https://kubernetes.io/docs/concepts/storage/persistent-volumes#access-modes-1")
    allocated_resources: Optional[Dict[str, StrictStr]] = Field(None, alias="allocatedResources", description="allocatedResources is the storage resource within AllocatedResources tracks the capacity allocated to a PVC. It may be larger than the actual capacity when a volume expansion operation is requested. For storage quota, the larger value from allocatedResources and PVC.spec.resources is used. If allocatedResources is not set, PVC.spec.resources alone is used for quota calculation. If a volume expansion capacity request is lowered, allocatedResources is only lowered if there are no expansion operations in progress and if the actual volume capacity is equal or lower than the requested capacity. This is an alpha field and requires enabling RecoverVolumeExpansionFailure feature.")
    capacity: Optional[Dict[str, StrictStr]] = Field(None, description="capacity represents the actual resources of the underlying volume.")
    conditions: Optional[List[V1PersistentVolumeClaimCondition]] = Field(None, description="conditions is the current Condition of persistent volume claim. If underlying persistent volume is being resized then the Condition will be set to 'ResizeStarted'.")
    phase: Optional[StrictStr] = Field(None, description="phase represents the current phase of PersistentVolumeClaim.  ")
    resize_status: Optional[StrictStr] = Field(None, alias="resizeStatus", description="resizeStatus stores status of resize operation. ResizeStatus is not set by default but when expansion is complete resizeStatus is set to empty string by resize controller or kubelet. This is an alpha field and requires enabling RecoverVolumeExpansionFailure feature.")

    class Config:
        allow_population_by_field_name = True
        validate_assignment = True

    def to_str(self) -> str:
        """Returns the string representation of the model using alias"""
        return pprint.pformat(self.to_dict())

    def to_json(self) -> str:
        """Returns the JSON representation of the model using alias"""
        return json.dumps(self.to_dict())

    @classmethod
    def from_json(cls, json_str: str) -> V1PersistentVolumeClaimStatus:
        """Create an instance of V1PersistentVolumeClaimStatus from a JSON string"""
        return cls.from_dict(json.loads(json_str))

    def to_dict(self):
        """Returns the dictionary representation of the model using alias"""
        _dict = self.dict(by_alias=True, exclude_none=True)
        # override the default output from pydantic by calling `to_dict()` of each item in conditions (list)
        _items = []
        if self.conditions:
            for _item in self.conditions:
                if _item:
                    _items.append(_item.to_dict())
            _dict['conditions'] = _items

        return _dict

    @classmethod
    def from_dict(cls, obj: dict) -> V1PersistentVolumeClaimStatus:
        """Create an instance of V1PersistentVolumeClaimStatus from a dict"""
        if type(obj) is not dict:
            return V1PersistentVolumeClaimStatus.parse_obj(obj)

        return V1PersistentVolumeClaimStatus.parse_obj({
            "access_modes": obj.get("accessModes"),
            "allocated_resources": obj.get("allocatedResources"),
            "capacity": obj.get("capacity"),
            "conditions": [V1PersistentVolumeClaimCondition.from_dict(_item) for _item in obj.get("conditions")],
            "phase": obj.get("phase"),
            "resize_status": obj.get("resizeStatus")
        })



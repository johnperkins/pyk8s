# coding: utf-8

"""
    Kubernetes

    No description provided (generated by Openapi Generator https://github.com/openapitools/openapi-generator)  # noqa: E501

    The version of the OpenAPI document: release-1.25
    Generated by: https://openapi-generator.tech
"""


from __future__ import annotations
from inspect import getfullargspec
import pprint
import re  # noqa: F401
import json


from typing import List, Optional
from pydantic import BaseModel, Field
from pyk8s.models.v1_for_zone import V1ForZone
from pydantic import ValidationError

class V1EndpointHints(BaseModel):
    """NOTE: This class is auto generated by OpenAPI Generator.
    Ref: https://openapi-generator.tech

    Do not edit the class manually.
    """
    for_zones: Optional[List[V1ForZone]] = Field(None, alias="forZones", description="forZones indicates the zone(s) this endpoint should be consumed by to enable topology aware routing.")

    class Config:
        allow_population_by_field_name = True
        validate_assignment = True

    def to_str(self) -> str:
        """Returns the string representation of the model using alias"""
        return pprint.pformat(self.to_dict())

    def to_json(self) -> str:
        """Returns the JSON representation of the model using alias"""
        return json.dumps(self.to_dict())

    @classmethod
    def from_json(cls, json_str: str) -> V1EndpointHints:
        """Create an instance of V1EndpointHints from a JSON string"""
        return cls.from_dict(json.loads(json_str))

    def to_dict(self):
        """Returns the dictionary representation of the model using alias"""
        _dict = self.dict(by_alias=True, exclude_none=True)
        # override the default output from pydantic by calling `to_dict()` of each item in for_zones (list)
        _items = []
        if self.for_zones:
            for _item in self.for_zones:
                if _item:
                    _items.append(_item.to_dict())
            _dict['forZones'] = _items

        return _dict

    @classmethod
    def from_dict(cls, obj: dict) -> V1EndpointHints:
        """Create an instance of V1EndpointHints from a dict"""
        if type(obj) is not dict:
            return V1EndpointHints.parse_obj(obj)

        return V1EndpointHints.parse_obj({
            "for_zones": [V1ForZone.from_dict(_item) for _item in obj.get("forZones")]
        })



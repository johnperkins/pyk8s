# coding: utf-8

"""
    Kubernetes

    No description provided (generated by Openapi Generator https://github.com/openapitools/openapi-generator)  # noqa: E501

    The version of the OpenAPI document: release-1.25
    Generated by: https://openapi-generator.tech
"""


from __future__ import annotations
from inspect import getfullargspec
import pprint
import re  # noqa: F401
import json


from typing import List, Optional
from pydantic import BaseModel, Field, StrictStr
from pyk8s.models.v1_volume_node_resources import V1VolumeNodeResources
from pydantic import ValidationError

class V1CSINodeDriver(BaseModel):
    """NOTE: This class is auto generated by OpenAPI Generator.
    Ref: https://openapi-generator.tech

    Do not edit the class manually.
    """
    allocatable: Optional[V1VolumeNodeResources] = None
    name: StrictStr = Field(..., description="This is the name of the CSI driver that this object refers to. This MUST be the same name returned by the CSI GetPluginName() call for that driver.")
    node_id: StrictStr = Field(..., alias="nodeID", description="nodeID of the node from the driver point of view. This field enables Kubernetes to communicate with storage systems that do not share the same nomenclature for nodes. For example, Kubernetes may refer to a given node as \"node1\", but the storage system may refer to the same node as \"nodeA\". When Kubernetes issues a command to the storage system to attach a volume to a specific node, it can use this field to refer to the node name using the ID that the storage system will understand, e.g. \"nodeA\" instead of \"node1\". This field is required.")
    topology_keys: Optional[List[StrictStr]] = Field(None, alias="topologyKeys", description="topologyKeys is the list of keys supported by the driver. When a driver is initialized on a cluster, it provides a set of topology keys that it understands (e.g. \"company.com/zone\", \"company.com/region\"). When a driver is initialized on a node, it provides the same topology keys along with values. Kubelet will expose these topology keys as labels on its own node object. When Kubernetes does topology aware provisioning, it can use this list to determine which labels it should retrieve from the node object and pass back to the driver. It is possible for different nodes to use different topology keys. This can be empty if driver does not support topology.")

    class Config:
        allow_population_by_field_name = True
        validate_assignment = True

    def to_str(self) -> str:
        """Returns the string representation of the model using alias"""
        return pprint.pformat(self.to_dict())

    def to_json(self) -> str:
        """Returns the JSON representation of the model using alias"""
        return json.dumps(self.to_dict())

    @classmethod
    def from_json(cls, json_str: str) -> V1CSINodeDriver:
        """Create an instance of V1CSINodeDriver from a JSON string"""
        return cls.from_dict(json.loads(json_str))

    def to_dict(self):
        """Returns the dictionary representation of the model using alias"""
        _dict = self.dict(by_alias=True, exclude_none=True)
        # override the default output from pydantic by calling `to_dict()` of allocatable
        if self.allocatable:
            _dict['allocatable'] = self.allocatable.to_dict()

        return _dict

    @classmethod
    def from_dict(cls, obj: dict) -> V1CSINodeDriver:
        """Create an instance of V1CSINodeDriver from a dict"""
        if type(obj) is not dict:
            return V1CSINodeDriver.parse_obj(obj)

        return V1CSINodeDriver.parse_obj({
            "allocatable": V1VolumeNodeResources.from_dict(obj.get("allocatable")),
            "name": obj.get("name"),
            "node_id": obj.get("nodeID"),
            "topology_keys": obj.get("topologyKeys")
        })



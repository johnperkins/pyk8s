# coding: utf-8

"""
    Kubernetes

    No description provided (generated by Openapi Generator https://github.com/openapitools/openapi-generator)  # noqa: E501

    The version of the OpenAPI document: release-1.25
    Generated by: https://openapi-generator.tech
"""


from __future__ import annotations
from inspect import getfullargspec
import pprint
import re  # noqa: F401
import json


from typing import List, Optional
from pydantic import BaseModel, Field, StrictStr
from pyk8s.models.v1_label_selector import V1LabelSelector
from pyk8s.models.v1_resource_requirements import V1ResourceRequirements
from pyk8s.models.v1_typed_local_object_reference import V1TypedLocalObjectReference
from pydantic import ValidationError

class V1PersistentVolumeClaimSpec(BaseModel):
    """NOTE: This class is auto generated by OpenAPI Generator.
    Ref: https://openapi-generator.tech

    Do not edit the class manually.
    """
    access_modes: Optional[List[StrictStr]] = Field(None, alias="accessModes", description="accessModes contains the desired access modes the volume should have. More info: https://kubernetes.io/docs/concepts/storage/persistent-volumes#access-modes-1")
    data_source: Optional[V1TypedLocalObjectReference] = Field(None, alias="dataSource")
    data_source_ref: Optional[V1TypedLocalObjectReference] = Field(None, alias="dataSourceRef")
    resources: Optional[V1ResourceRequirements] = None
    selector: Optional[V1LabelSelector] = None
    storage_class_name: Optional[StrictStr] = Field(None, alias="storageClassName", description="storageClassName is the name of the StorageClass required by the claim. More info: https://kubernetes.io/docs/concepts/storage/persistent-volumes#class-1")
    volume_mode: Optional[StrictStr] = Field(None, alias="volumeMode", description="volumeMode defines what type of volume is required by the claim. Value of Filesystem is implied when not included in claim spec.")
    volume_name: Optional[StrictStr] = Field(None, alias="volumeName", description="volumeName is the binding reference to the PersistentVolume backing this claim.")

    class Config:
        allow_population_by_field_name = True
        validate_assignment = True

    def to_str(self) -> str:
        """Returns the string representation of the model using alias"""
        return pprint.pformat(self.to_dict())

    def to_json(self) -> str:
        """Returns the JSON representation of the model using alias"""
        return json.dumps(self.to_dict())

    @classmethod
    def from_json(cls, json_str: str) -> V1PersistentVolumeClaimSpec:
        """Create an instance of V1PersistentVolumeClaimSpec from a JSON string"""
        return cls.from_dict(json.loads(json_str))

    def to_dict(self):
        """Returns the dictionary representation of the model using alias"""
        _dict = self.dict(by_alias=True, exclude_none=True)
        # override the default output from pydantic by calling `to_dict()` of data_source
        if self.data_source:
            _dict['dataSource'] = self.data_source.to_dict()
        # override the default output from pydantic by calling `to_dict()` of data_source_ref
        if self.data_source_ref:
            _dict['dataSourceRef'] = self.data_source_ref.to_dict()
        # override the default output from pydantic by calling `to_dict()` of resources
        if self.resources:
            _dict['resources'] = self.resources.to_dict()
        # override the default output from pydantic by calling `to_dict()` of selector
        if self.selector:
            _dict['selector'] = self.selector.to_dict()

        return _dict

    @classmethod
    def from_dict(cls, obj: dict) -> V1PersistentVolumeClaimSpec:
        """Create an instance of V1PersistentVolumeClaimSpec from a dict"""
        if type(obj) is not dict:
            return V1PersistentVolumeClaimSpec.parse_obj(obj)

        return V1PersistentVolumeClaimSpec.parse_obj({
            "access_modes": obj.get("accessModes"),
            "data_source": V1TypedLocalObjectReference.from_dict(obj.get("dataSource")),
            "data_source_ref": V1TypedLocalObjectReference.from_dict(obj.get("dataSourceRef")),
            "resources": V1ResourceRequirements.from_dict(obj.get("resources")),
            "selector": V1LabelSelector.from_dict(obj.get("selector")),
            "storage_class_name": obj.get("storageClassName"),
            "volume_mode": obj.get("volumeMode"),
            "volume_name": obj.get("volumeName")
        })



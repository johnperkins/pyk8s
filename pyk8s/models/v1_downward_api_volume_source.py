# coding: utf-8

"""
    Kubernetes

    No description provided (generated by Openapi Generator https://github.com/openapitools/openapi-generator)  # noqa: E501

    The version of the OpenAPI document: release-1.25
    Generated by: https://openapi-generator.tech
"""


from __future__ import annotations
from inspect import getfullargspec
import pprint
import re  # noqa: F401
import json


from typing import List, Optional
from pydantic import BaseModel, Field, StrictInt
from pyk8s.models.v1_downward_api_volume_file import V1DownwardAPIVolumeFile
from pydantic import ValidationError

class V1DownwardAPIVolumeSource(BaseModel):
    """NOTE: This class is auto generated by OpenAPI Generator.
    Ref: https://openapi-generator.tech

    Do not edit the class manually.
    """
    default_mode: Optional[StrictInt] = Field(None, alias="defaultMode", description="Optional: mode bits to use on created files by default. Must be a Optional: mode bits used to set permissions on created files by default. Must be an octal value between 0000 and 0777 or a decimal value between 0 and 511. YAML accepts both octal and decimal values, JSON requires decimal values for mode bits. Defaults to 0644. Directories within the path are not affected by this setting. This might be in conflict with other options that affect the file mode, like fsGroup, and the result can be other mode bits set.")
    items: Optional[List[V1DownwardAPIVolumeFile]] = Field(None, description="Items is a list of downward API volume file")

    class Config:
        allow_population_by_field_name = True
        validate_assignment = True

    def to_str(self) -> str:
        """Returns the string representation of the model using alias"""
        return pprint.pformat(self.to_dict())

    def to_json(self) -> str:
        """Returns the JSON representation of the model using alias"""
        return json.dumps(self.to_dict())

    @classmethod
    def from_json(cls, json_str: str) -> V1DownwardAPIVolumeSource:
        """Create an instance of V1DownwardAPIVolumeSource from a JSON string"""
        return cls.from_dict(json.loads(json_str))

    def to_dict(self):
        """Returns the dictionary representation of the model using alias"""
        _dict = self.dict(by_alias=True, exclude_none=True)
        # override the default output from pydantic by calling `to_dict()` of each item in items (list)
        _items = []
        if self.items:
            for _item in self.items:
                if _item:
                    _items.append(_item.to_dict())
            _dict['items'] = _items

        return _dict

    @classmethod
    def from_dict(cls, obj: dict) -> V1DownwardAPIVolumeSource:
        """Create an instance of V1DownwardAPIVolumeSource from a dict"""
        if type(obj) is not dict:
            return V1DownwardAPIVolumeSource.parse_obj(obj)

        return V1DownwardAPIVolumeSource.parse_obj({
            "default_mode": obj.get("defaultMode"),
            "items": [V1DownwardAPIVolumeFile.from_dict(_item) for _item in obj.get("items")]
        })



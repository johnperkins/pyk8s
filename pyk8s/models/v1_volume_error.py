# coding: utf-8

"""
    Kubernetes

    No description provided (generated by Openapi Generator https://github.com/openapitools/openapi-generator)  # noqa: E501

    The version of the OpenAPI document: release-1.25
    Generated by: https://openapi-generator.tech
"""


from __future__ import annotations
from inspect import getfullargspec
import pprint
import re  # noqa: F401
import json

from datetime import datetime
from typing import Optional
from pydantic import BaseModel, Field, StrictStr
from pydantic import ValidationError

class V1VolumeError(BaseModel):
    """NOTE: This class is auto generated by OpenAPI Generator.
    Ref: https://openapi-generator.tech

    Do not edit the class manually.
    """
    message: Optional[StrictStr] = Field(None, description="String detailing the error encountered during Attach or Detach operation. This string may be logged, so it should not contain sensitive information.")
    time: Optional[datetime] = Field(None, description="Time the error was encountered.")

    class Config:
        allow_population_by_field_name = True
        validate_assignment = True

    def to_str(self) -> str:
        """Returns the string representation of the model using alias"""
        return pprint.pformat(self.to_dict())

    def to_json(self) -> str:
        """Returns the JSON representation of the model using alias"""
        return json.dumps(self.to_dict())

    @classmethod
    def from_json(cls, json_str: str) -> V1VolumeError:
        """Create an instance of V1VolumeError from a JSON string"""
        return cls.from_dict(json.loads(json_str))

    def to_dict(self):
        """Returns the dictionary representation of the model using alias"""
        _dict = self.dict(by_alias=True, exclude_none=True)

        return _dict

    @classmethod
    def from_dict(cls, obj: dict) -> V1VolumeError:
        """Create an instance of V1VolumeError from a dict"""
        if type(obj) is not dict:
            return V1VolumeError.parse_obj(obj)

        return V1VolumeError.parse_obj({
            "message": obj.get("message"),
            "time": obj.get("time")
        })



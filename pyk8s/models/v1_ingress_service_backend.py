# coding: utf-8

"""
    Kubernetes

    No description provided (generated by Openapi Generator https://github.com/openapitools/openapi-generator)  # noqa: E501

    The version of the OpenAPI document: release-1.25
    Generated by: https://openapi-generator.tech
"""


from __future__ import annotations
from inspect import getfullargspec
import pprint
import re  # noqa: F401
import json


from typing import Optional
from pydantic import BaseModel, Field, StrictStr
from pyk8s.models.v1_service_backend_port import V1ServiceBackendPort
from pydantic import ValidationError

class V1IngressServiceBackend(BaseModel):
    """NOTE: This class is auto generated by OpenAPI Generator.
    Ref: https://openapi-generator.tech

    Do not edit the class manually.
    """
    name: StrictStr = Field(..., description="Name is the referenced service. The service must exist in the same namespace as the Ingress object.")
    port: Optional[V1ServiceBackendPort] = None

    class Config:
        allow_population_by_field_name = True
        validate_assignment = True

    def to_str(self) -> str:
        """Returns the string representation of the model using alias"""
        return pprint.pformat(self.to_dict())

    def to_json(self) -> str:
        """Returns the JSON representation of the model using alias"""
        return json.dumps(self.to_dict())

    @classmethod
    def from_json(cls, json_str: str) -> V1IngressServiceBackend:
        """Create an instance of V1IngressServiceBackend from a JSON string"""
        return cls.from_dict(json.loads(json_str))

    def to_dict(self):
        """Returns the dictionary representation of the model using alias"""
        _dict = self.dict(by_alias=True, exclude_none=True)
        # override the default output from pydantic by calling `to_dict()` of port
        if self.port:
            _dict['port'] = self.port.to_dict()

        return _dict

    @classmethod
    def from_dict(cls, obj: dict) -> V1IngressServiceBackend:
        """Create an instance of V1IngressServiceBackend from a dict"""
        if type(obj) is not dict:
            return V1IngressServiceBackend.parse_obj(obj)

        return V1IngressServiceBackend.parse_obj({
            "name": obj.get("name"),
            "port": V1ServiceBackendPort.from_dict(obj.get("port"))
        })



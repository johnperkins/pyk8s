# coding: utf-8

"""
    Kubernetes

    No description provided (generated by Openapi Generator https://github.com/openapitools/openapi-generator)  # noqa: E501

    The version of the OpenAPI document: release-1.25
    Generated by: https://openapi-generator.tech
"""


from __future__ import annotations
from inspect import getfullargspec
import pprint
import re  # noqa: F401
import json


from typing import Optional
from pydantic import BaseModel, Field, StrictInt, StrictStr
from pydantic import ValidationError

class StorageV1TokenRequest(BaseModel):
    """NOTE: This class is auto generated by OpenAPI Generator.
    Ref: https://openapi-generator.tech

    Do not edit the class manually.
    """
    audience: StrictStr = Field(..., description="Audience is the intended audience of the token in \"TokenRequestSpec\". It will default to the audiences of kube apiserver.")
    expiration_seconds: Optional[StrictInt] = Field(None, alias="expirationSeconds", description="ExpirationSeconds is the duration of validity of the token in \"TokenRequestSpec\". It has the same default value of \"ExpirationSeconds\" in \"TokenRequestSpec\".")

    class Config:
        allow_population_by_field_name = True
        validate_assignment = True

    def to_str(self) -> str:
        """Returns the string representation of the model using alias"""
        return pprint.pformat(self.to_dict())

    def to_json(self) -> str:
        """Returns the JSON representation of the model using alias"""
        return json.dumps(self.to_dict())

    @classmethod
    def from_json(cls, json_str: str) -> StorageV1TokenRequest:
        """Create an instance of StorageV1TokenRequest from a JSON string"""
        return cls.from_dict(json.loads(json_str))

    def to_dict(self):
        """Returns the dictionary representation of the model using alias"""
        _dict = self.dict(by_alias=True, exclude_none=True)

        return _dict

    @classmethod
    def from_dict(cls, obj: dict) -> StorageV1TokenRequest:
        """Create an instance of StorageV1TokenRequest from a dict"""
        if type(obj) is not dict:
            return StorageV1TokenRequest.parse_obj(obj)

        return StorageV1TokenRequest.parse_obj({
            "audience": obj.get("audience"),
            "expiration_seconds": obj.get("expirationSeconds")
        })


